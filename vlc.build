<project default="build" xmlns="schemas/ea/framework3.xsd">
    <!-- Provide an optionset with the base options for C and C++ without
         our forced allocators. This will be inherited by the VlcC11/CppSources option sets
         which will provide the forced include -->
    <optionset name="VlcForcedAllocatorBypassC" fromoptionset="${EngineStaticOrShared}">
        <option name="cc.options" if="${config-compiler}==clang">
            ${option.value}
            -std=c11
        </option>
	</optionset>

	<optionset name="VlcForcedAllocatorBypass" fromoptionset="${EngineStaticOrShared}">
		<option name="cc.options" if="${config-compiler}==vc">
            ${option.value}
            /GR
            /EHsc
        </option>
        <option name="cc.options" if="${config-compiler}==clang">
            ${option.value}
            -frtti
            -fexceptions
        </option>
	</optionset>

    <!-- Add our forced allocator forced include for the most common files -->
    <optionset name="VlcC11Sources" fromoptionset="VlcForcedAllocatorBypassC">
        <option name="cc.options" if="${config-compiler} == 'vc'">
            ${option.value}
            -FI${package.dir}/vlc/include/forced_allocator.h
        </option>
        <option name="cc.options" if="${config-compiler} == 'clang'">
            ${option.value}
            --include=${package.dir}/vlc/include/forced_allocator.h
        </option>
    </optionset>

    <optionset name="VlcCppSources"  fromoptionset="VlcForcedAllocatorBypass">
        <option name="cc.options" if="${config-compiler} == 'vc'">
            ${option.value}
            -FI${package.dir}/vlc/include/forced_allocator.h
        </option>
        <option name="cc.options" if="${config-compiler} == 'clang'">
            ${option.value}
            --include=${package.dir}/vlc/include/forced_allocator.h
        </option>
    </optionset>

	<choose>
		<do if="${package.MovieVLC.build-from-source??false}">
    <DynamicLibrary name="Engine.MovieVLC.LibVLC">
        <config>
            <option name="use-pch" value="no"/>
            <option name="bulkbuild" value="off"/>
			<pch enable="false"/>
			<option name="warnings" value="off"/>
			<defines>
				HAVE_CONFIG_H=1
				DLL_EXPORT
                MERGE_FFMPEG
                __PLUGIN__
				FB_VLC_MODULES_AGGREGATE
				FB_MOVIEVLC_LIBVLC_EXPORTS <!-- always built as a DLL -->
                MODULE_NAME=core
				MODULE_STRING=\"core\"
				<do if="${config-system} == pc64 or ${config-system} == pc or ${config-system} == xbsx or ${config-system} == capilano">
					VLC_WINSTORE_APP=1
					<do if="${movie.libvlc.fileplugin??false}">
						VLC_FILE_PLUGIN_ENABLED=1
					</do>
				</do>
				<do if="${movie.libvlc.mp4plugin??false}">
					VLC_MP4_PLUGIN_ENABLED=1
				</do>
				<do if="${movie.libvlc.ffmpeg??${config-system} == pc64}">
					HAVE_LIBAVCODEC_AVCODEC_H=1
					HAVE_LIBAVUTIL_AVUTIL_H=1
					HAVE_LIBAVFORMAT_AVFORMAT_H=1
					<do if="${config-system} == pc64 or ${config-system} == xbsx or ${config-system} == capilano">
						HAVE_LIBAVCODEC_D3D12VA_H=1
					</do>
					<!-- HAVE_LIBAVFORMAT_AVIO_H=1 -->
					<!-- HAVE_LIBSWSCALE_SWSCALE_H=1 -->
				</do>
            </defines>
            <warningsuppression>
                <do if="${config-compiler} == clang">
                    -Wno-pointer-sign
                    -Wno-switch
                    -Wno-implicit-int-float-conversion
                    -Wno-logical-op-parentheses
                    -Wno-parentheses
                    -Wno-deprecated-declarations
                    -Wno-incompatible-pointer-types-discards-qualifiers
                    -Wno-visibility
                    -Wno-invalid-source-encoding
                    -Wno-int-conversion
                    -Wno-incompatible-pointer-types
                    -Wno-unused-variable
                </do>
            </warningsuppression>
        </config>
		<dependencies>
			<auto>
				<do if="${movie.libvlc.ffmpeg??${config-system} == pc64}">
				MovieVLC/Engine.MovieVLC.Contrib.avcodec
				MovieVLC/Engine.MovieVLC.Contrib.avformat
				MovieVLC/Engine.MovieVLC.Contrib.avutil
				MovieVLC/Engine.MovieVLC.Contrib.swscale
				</do>

				MovieVLC/Engine.MovieVLC.Contrib.libdvbpsi

				MovieVLC/Engine.MovieVLC.Contrib.libxml

				MovieVLC/Engine.MovieVLC.Contrib.soxr
			</auto>
		</dependencies>
		<headerfiles>
			<includes name="${package.dir}/vlc/include/*.h" />
			<includes name="${package.dir}/vlc/include/compat/**/*.h" />
			<includes name="${package.dir}/vlc/include/config/**/config.h" />
			<includes name="${package.dir}/vlc/include/modules/**/*.h" />

            <!-- Adaptive streaming module -->
			<includes name="modules/demux/adaptive/**/*.h" />
			<includes name="modules/demux/adaptive/**/*.hpp" />
            <excludes name="modules/demux/adaptive/test/**/*.hpp" />
            <excludes name="modules/demux/adaptive/http/Transport.hpp" />
			<includes name="modules/demux/mp4/libmp4.h" />
			<includes name="modules/meta_engine/ID3Tag.h" />
			<includes name="modules/demux/dash/**/*.h" />
			<includes name="modules/demux/dash/**/*.hpp" />
			<includes name="modules/demux/hls/**/*.h" />
			<includes name="modules/demux/hls/**/*.hpp" />
			<includes name="modules/mux/mp4/libmp4mux.h" />
            <excludes name="modules/demux/adaptive/test/**/*.hpp" />

			<!-- packetizers -->
			<includes name="modules/packetizer/h264_slice.h" />
			<includes name="modules/packetizer/hxxx_common.h" />
			<includes name="modules/packetizer/hxxx_nal.h" />
			<includes name="modules/packetizer/hxxx_sei.h" />
			<do if="${config-system} == kettle or ${config-system} == ps5">
				<includes name="modules/packetizer/hxxx_ep3b.h" />
				<includes name="modules/packetizer/packetizer_helper.h" />
				<includes name="modules/packetizer/startcode_helper.h" />
			</do>

			<do if="${movie.libvlc.ffmpeg??${config-system} == pc64}">
			<!-- FFmpeg module -->
			<includes name="modules/codec/avcodec/avcommon.h" />
			<includes name="modules/codec/avcodec/chroma.h" />
			<includes name="modules/codec/avcodec/avcommon_compat.h" />
			<includes name="modules/codec/avcodec/va.h" />
			<includes name="modules/codec/avcodec/avcodec.h" />
			<includes name="modules/codec/avcodec/directx_va.h" />
			<includes name="modules/codec/avcodec/va_surface.h" />
			</do>

			<!-- TS module -->
			<includes name="modules/demux/mpeg/*.h" />
			<includes name="modules/demux/dvb-text.h" />
			<includes name="modules/demux/opus.h" />
			<includes name="modules/access/dtv/en50221_capmt.h" />
			<includes name="modules/codec/jpeg2000.h" />
			<includes name="modules/codec/scte18.h" />
			<includes name="modules/codec/atsc_a65.h" />

			<do if="${movie.libvlc.mp4plugin??false}">
				<includes name="modules/demux/mp4/*.h"/>
				<includes name="modules/demux/asf/asfpacket.h"/>
			</do>

			<!-- webvtt codec -->
			<includes name="modules/codec/substext.h"/>
			<includes name="include/modules/codec/webvtt/webvtt.h" />

			<!-- chroma converters modules -->
			<includes name="modules/video_chroma/dxgi_fmt.h" />

			<!-- D3D12 modules -->
			<includes name="modules/hw/d3d12/d3d12_fmt.h"/>
			<includes name="modules/codec/mft_d3d12.h"/>
		</headerfiles>
		<includedirs>
			${package.dir}/vlc/include/
			${package.dir}/vlc/include/compat/${config-system}/
			${package.dir}/vlc/include/config/${config-system}/


			${package.dir}/vlc/src/

			${package.dir}/vlc/modules/demux/adaptive/
			${package.dir}/include

			<do if="${config-system} == xbsx or ${config-system} == capilano">
				${eaconfig.PlatformSDK.dir}/gamekit/Include/mf_x
			</do>
        </includedirs>
		<sourcefiles sort="false">
            <!-- LibVLC core -->
            <includes name="src/audio_output/*.c" optionset="VlcC11Sources" />
			<includes name="src/clock/*.c" optionset="VlcC11Sources" />
			<includes name="src/config/*.c" optionset="VlcC11Sources" />
            <excludes name="src/config/file.c" />
            <excludes name="src/config/help.c" />
			<includes name="src/extras/*.c" optionset="VlcC11Sources" />
			<includes name="src/input/*.c" optionset="VlcC11Sources" />
			<excludes name="src/input/vlm.c" />
			<includes name="src/interface/*.c" optionset="VlcC11Sources" />
            <!-- Explicitly include both allocators files with the appropriate optionset -->
            <includes name="src/misc/crt_allocator.c" optionset="VlcForcedAllocatorBypassC" />
            <includes name="src/misc/std_allocator.cpp" optionset="VlcForcedAllocatorBypass" />
            <includes name="src/misc/*.c" optionset="VlcC11Sources" />
            <includes name="src/misc/allocator.c" optionset="VlcC11Sources" />
			<excludes name="src/misc/update.c" />
			<excludes name="src/misc/update_crypto.c" />
			<excludes name="src/misc/fourcc_gen.c" />
			<includes name="src/modules/*.c" optionset="VlcC11Sources"/>
            <excludes name="src/modules/cache.c" />
			<includes name="src/player/*.c" optionset="VlcC11Sources"/>
			<includes name="src/playlist/*.c" optionset="VlcC11Sources"/>
			<excludes name="src/playlist/test.c" />
            <excludes name="src/playlist/export.c" />
            <do if="${config-system} == ps5 or ${config-system} == kettle">
                <includes name="src/ps5/error.c" optionset="VlcC11Sources"/>
                <includes name="src/ps5/sort.c" optionset="VlcC11Sources"/>
                <includes name="src/ps5/thread.c" optionset="VlcC11Sources"/>
                <includes name="src/ps5/rand.c" optionset="VlcC11Sources"/>
                <includes name="src/ps5/specific.c" optionset="VlcC11Sources"/>
                <includes name="src/posix/timer.c" optionset="VlcC11Sources"/>
                <includes name="src/posix/wait.c" optionset="VlcC11Sources"/>
            </do>
            <includes name="src/preparser/preparser.c" optionset="VlcC11Sources"/>
			<includes name="src/text/*.c" optionset="VlcC11Sources"/>
			<includes name="src/video_output/*.c" optionset="VlcC11Sources"/>
			<includes name="src/win32/*.c" if="${config-system} == pc64 or ${config-system} == xbsx or ${config-system} == capilano" optionset="VlcC11Sources" />
			<includes name="src/libvlc.c" optionset="VlcC11Sources"/>
			<includes name="src/missing.c" optionset="VlcC11Sources"/>
			<includes name="src/revision.c" optionset="VlcC11Sources"/>
			<includes name="src/version.c" optionset="VlcC11Sources"/>

            <!-- modules -->
            <!-- Adaptive module -->
			<includes name="modules/demux/adaptive/adaptive.cpp" optionset="VlcCppSources"/>
			<includes name="modules/demux/adaptive/**/*.cpp" optionset="VlcCppSources"/>
            <excludes name="modules/demux/adaptive/http/Transport.cpp" />

			<includes name="modules/demux/mp4/libmp4.c" optionset="VlcC11Sources"/>
			<includes name="modules/demux/dash/**/*.cpp" optionset="VlcCppSources" />
			<includes name="modules/demux/hls/**/*.cpp" optionset="VlcCppSources" />
			<includes name="modules/demux/smooth/**/*.cpp" optionset="VlcCppSources" />
            <excludes name="modules/demux/adaptive/test/**/*.cpp" />
			<includes name="modules/mux/mp4/libmp4mux.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/h264_nal.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/hevc_nal.c" optionset="VlcC11Sources"/>

			<includes name="modules/packetizer/mpeg4audio.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/streaming.c" optionset="VlcC11Sources"/>


			<!-- HXXX helpers -->
			<includes name="modules/codec/hxxx_helper.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/h264.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/h264_slice.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/h264_nal.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/hevc_nal.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/hxxx_nal.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/hxxx_common.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/hxxx_sei.c" optionset="VlcC11Sources"/>

			<do if="${movie.libvlc.ffmpeg??${config-system} == pc64}">
			<!-- FFmpeg module -->
			<includes name="modules/codec/avcodec/fourcc.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/avcodec/chroma.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/avcodec/video.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/avcodec/subtitle.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/avcodec/audio.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/avcodec/va.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/avcodec/avcodec.c" optionset="VlcC11Sources"/>
			<includes name="modules/packetizer/avparser.c" optionset="VlcC11Sources"/>
			<includes name="modules/demux/avformat/demux.c" optionset="VlcC11Sources"/>
				<includes name="modules/codec/avcodec/directx_va.c" optionset="VlcC11Sources"/>
				<includes name="modules/codec/avcodec/va_surface.c" optionset="VlcC11Sources"/>
			</do>
			<do if="${config-system} == pc64 or ${config-system} == xbsx or ${config-system} == capilano">
				<includes name="modules/codec/mft.cpp" optionset="VlcCppSources"/>
				<includes name="modules/codec/mft_d3d12.cpp" optionset="VlcCppSources"/>
			</do>


			<!-- TS demuxer module -->
			<includes name="modules/demux/mpeg/ts*.c" optionset="VlcC11Sources"/>
			<includes name="modules/demux/mpeg/sections.c" optionset="VlcC11Sources"/>
			<includes name="modules/demux/mpeg/mpeg4_iod.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/atsc_a65.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/opus_header.c" optionset="VlcC11Sources"/>
			<includes name="modules/mux/mpeg/csa.c" optionset="VlcC11Sources"/>
			<includes name="modules/mux/mpeg/tables.c" optionset="VlcC11Sources"/>
			<includes name="modules/mux/mpeg/tsutil.c" optionset="VlcC11Sources"/>

			<!-- webvtt codec -->

			<includes name="modules/codec/webvtt/subsvtt.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/webvtt/subsvttext.c" optionset="VlcC11Sources"/>
			<includes name="modules/codec/webvtt/webvtt.c" optionset="VlcC11Sources"/>
			<includes name="modules/demux/webvtt.c" optionset="VlcC11Sources"/>
			<includes name="modules/text_renderer/expeller.c" optionset="VlcC11Sources"/>
			<!-- LibXML2 module -->
			<includes name="modules/misc/xml/libxml.c" optionset="VlcC11Sources"/>

			<!-- Soxr module -->
			<includes name="modules/audio_filter/resampler/soxr.c" optionset="VlcC11Sources"/>

			<!-- video filter modules -->
			<do if="${movie.libvlc.ffmpeg??${config-system} == pc64}">
			<includes name="modules/video_chroma/swscale.c" optionset="VlcC11Sources"/>
			</do>
			<includes name="modules/video_chroma/dxgi_fmt.c" optionset="VlcC11Sources" if="${config-system} == pc64 or ${config-system} == pc or ${config-system} == xbsx or ${config-system} == capilano"/>

			<!-- vout module -->
			<includes name="modules/video_output/vmem.c" optionset="VlcC11Sources"/>
			<includes name="modules/video_output/wdummy.c" optionset="VlcC11Sources"/>
			<includes name="modules/video_output/wextern.c" optionset="VlcC11Sources" />

			<!-- D3D12 modules -->
			<do if="${config-system} == pc64 or ${config-system} == pc or ${config-system} == xbsx or ${config-system} == capilano">
				<includes name="modules/hw/d3d12/d3d12_device.cpp" optionset="VlcCppSources" />
				<includes name="modules/hw/d3d12/d3d12_fmt.cpp" optionset="VlcCppSources" />
				<do if="${movie.libvlc.ffmpeg??${config-system} == pc64}">
				<includes name="modules/codec/avcodec/d3d12va.cpp" optionset="VlcCppSources"/>
				</do>
				<includes name="modules/video_output/win32/directx12.cpp" optionset="VlcCppSources" />
			</do>

			<do if="${config-system} == pc64 or ${config-system} == pc or ${config-system} == xbsx or ${config-system} == capilano">
				<do if="${movie.libvlc.fileplugin??false}">
					<!-- FileInput -->
					<includes name="modules/access/file.c" optionset="VlcC11Sources"/>
					<includes name="modules/access/directory.c" optionset="VlcC11Sources"/>
				</do>
			</do>

			<do if="${movie.libvlc.mp4plugin??false}">
				<includes name="modules/demux/mp4/*.c" optionset="VlcC11Sources"/>
				<includes name="modules/demux/asf/asfpacket.c" optionset="VlcC11Sources"/>
			</do>

			<!-- Aggregate modules together -->
			<includes name="modules/modules_aggregate.c" optionset="VlcC11Sources"/>

			<includes name="lib/*.c" optionset="VlcC11Sources"/>

			<!-- Compat files -->
			<includes name="compat/asprintf.c" optionset="VlcC11Sources" />
			<includes name="compat/vasprintf.c" optionset="VlcC11Sources" />
			<includes name="compat/strndup.c" optionset="VlcC11Sources" />
			<includes name="compat/strsep.c" optionset="VlcC11Sources" />
			<includes name="compat/strtok_r.c" optionset="VlcC11Sources" />
			<includes name="compat/tfind.c" optionset="VlcC11Sources" />
			<includes name="compat/strcasestr.c" optionset="VlcC11Sources" />
			<includes name="compat/strnstr.c" optionset="VlcC11Sources" />
			<includes name="compat/strlcpy.c" optionset="VlcC11Sources" />
			<includes name="compat/strverscmp.c" optionset="VlcC11Sources" />
			<includes name="compat/nrand48.c" optionset="VlcC11Sources" />
			<includes name="compat/localtime_r.c" optionset="VlcC11Sources" />
			<includes name="compat/realpath.c" optionset="VlcC11Sources" />
			<includes name="compat/gmtime_r.c" optionset="VlcC11Sources" />
			<includes name="compat/memrchr.c" optionset="VlcC11Sources" />
			<includes name="compat/flockfile.c" optionset="VlcC11Sources" />
			<includes name="compat/timegm.c" optionset="VlcC11Sources" />
			<do if="${config-system} == kettle || ${config-system} == ps5">
				<includes name="compat/timespec_get.c" optionset="VlcC11Sources" />
				<includes name="compat/clock_nanosleep.c" optionset="VlcC11Sources" />
			</do>
		</sourcefiles>
		<libraries>
            <includes name="ws2_32.lib" asis="true" if="${config-system} == pc64"/>
            <includes name="shell32.lib" asis="true"  if="${config-system} == pc64"/>
            <includes name="normaliz.lib" asis="true" if="${config-system} == pc64"/>
            <includes name="Bcrypt.lib" asis="true" if="${config-system} == pc64" />
            <!-- For MediaFoundation -->
            <do if="${config-system} == capilano or ${config-system} == xbsx or ${config-system} == pc64">
                <includes name="ole32.lib" asis="true" if="${config-system} == pc64" />
                <includes name="Mfplat.lib" asis="true"/>
                <includes name="mfuuid.lib" asis="true"/>
            </do>
		</libraries>
		<bulkbuild enable="false"/>
	</DynamicLibrary>
		</do>
		<do>
			<runtime>
				<Utility name="Engine.MovieVLC.LibVLC"/>
			</runtime>
		</do>
	</choose>

	<target name="deploy">
		<property name="bindir" value="@{GetModuleOutputDirEx('bin', '${package.name}', 'Engine.MovieVLC.LibVLC', 'runtime')}" />
		<property name="libdir" value="@{GetModuleOutputDirEx('lib', '${package.name}', 'Engine.MovieVLC.LibVLC', 'runtime')}" />
		<property name="deployTarget" local="true" value="${package.MovieVLC.output-dir}" />
		<p4revert files="${deployTarget}/..."/>

		<!-- only available as a DLL -->
		<fileset name="bin-files" >
			<includes name="${bindir}/*${dll-suffix}"/>
			<includes name="${bindir}/*.pdb" if="${config-compiler}== vc"/>
		</fileset>
		<foreach item="FileSet" in="bin-files" property="File">
			<property name="DestFilePath" value="${deployTarget}/@{PathGetFileName(${File})}"/>
			<copy file="${File}" tofile="${DestFilePath}" overwrite="true" clobber="true" failonerror="true"/>
		</foreach>

		<!-- <fileset name="lib-files">
			<includes name="${libdir}/*${lib-suffix}"/>
			<includes name="${libdir}/*.pdb" if="${config-compiler} == vc"/>
		</fileset>

		<foreach item="FileSet" in="lib-files" property="File">
			<property name="DestFilePath" value="${deployTarget}/@{PathGetFileName(${File})}"/>
			<copy file="${File}" tofile="${DestFilePath}" overwrite="true" clobber="true" failonerror="true"/>
		</foreach> -->
		<p4 command="reconcile ${deployTarget}/..."/>
	</target>
</project>
