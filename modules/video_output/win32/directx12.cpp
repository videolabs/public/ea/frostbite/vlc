/*****************************************************************************
 * direct3d12.c: Windows DirectX12 video output module
 *****************************************************************************
 * Copyright (C) 2021 VLC authors and VideoLAN
 *
 * Authors: Steve Lhomme <robux4@videolabs.io>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_vout_display.h>

#include <vlc/libvlc.h>
#include <vlc/libvlc_picture.h>
#include <vlc/libvlc_media.h>
#include <vlc/libvlc_renderer_discoverer.h>
#include <vlc/libvlc_media_player.h>

#include <assert.h>

#include "../../hw/d3d12/d3d12_fmt.h"

EXTERN_SYMBOL int OpenDX12Display(vout_display_t *, video_format_t *, vlc_video_context *);

#ifndef FB_VLC_MODULES_AGGREGATE
vlc_module_begin ()
    set_shortname("DirectX12")
    set_description(N_("DirectX12 video output"))
    set_category(CAT_VIDEO)
    set_subcategory(SUBCAT_VIDEO_VOUT)

    add_shortcut("directx12")
    set_callback_display(OpenDX12Display, 350)
vlc_module_end ()
#endif

struct vlc_d3d12_plane_raw
{
    ID3D12Resource *resource;
    UINT           array_slice;
    void           (*used_in_queue)(void*, ID3D12CommandQueue &, bool used);
    void           *opaque;
};


struct vout_display_sys_t
{
    d3d12_device_t           *d3d_dev = nullptr;
    vlc_video_context        *vctx_in = nullptr;

    const d3d_format_t       *picQuadFmt = nullptr;

    /* outside rendering */
    void *outside_opaque = nullptr;
    libvlc_video_update_output_cb            updateOutputCb = nullptr;
    libvlc_video_swap_cb                     swapCb = nullptr;
    libvlc_video_makeCurrent_cb              startEndRenderingCb = nullptr;
    libvlc_video_frameMetadata_cb            sendMetadataCb = nullptr;
    libvlc_video_output_select_plane_cb      selectPlaneCb = nullptr;


    libvlc_video_output_spumetadata_cb spumetadataCb = nullptr;
    void* spumetadataUserData = nullptr;
};


static int UpdateDisplayFormat(vout_display_t *vd, const video_format_t *fmt)
{
    vout_display_sys_t* sys = static_cast<vout_display_sys_t*>(vd->sys);
    libvlc_video_render_cfg_t cfg = { 0 };

    cfg.width  = fmt->i_visible_width;
    cfg.height = fmt->i_visible_height;

    switch (fmt->i_chroma)
    {
    case VLC_CODEC_D3D12_OPAQUE:
        cfg.bitdepth = 8;
        break;
    case VLC_CODEC_D3D12_OPAQUE_RGBA:
    case VLC_CODEC_D3D12_OPAQUE_BGRA:
        cfg.bitdepth = 8;
        break;
    case VLC_CODEC_D3D12_OPAQUE_10B:
        cfg.bitdepth = 10;
        break;
    default:
        {
            const vlc_chroma_description_t *p_format = vlc_fourcc_GetChromaDescription(fmt->i_chroma);
            if (p_format == nullptr)
            {
                cfg.bitdepth = 8;
            }
            else
            {
                cfg.bitdepth = p_format->pixel_bits == 0 ? 8 : p_format->pixel_bits /
                                                               (p_format->plane_count==1 ? p_format->pixel_size : 1);
            }
        }
        break;
    }
    cfg.full_range = fmt->color_range == COLOR_RANGE_FULL;
    cfg.primaries  = (libvlc_video_color_primaries_t) fmt->primaries;
    cfg.colorspace = (libvlc_video_color_space_t)     fmt->space;
    cfg.transfer   = (libvlc_video_transfer_func_t)   fmt->transfer;

    libvlc_video_output_cfg_t out;
    if (!sys->updateOutputCb( sys->outside_opaque, &cfg, &out ))
    {
        msg_Err(vd, "Failed to set format %dx%d %d bits on output", cfg.width, cfg.height, cfg.bitdepth);
        return VLC_EGENERIC;
    }

    return VLC_SUCCESS;
}

static int Control(vout_display_t *vd, int query)
{
    vout_display_sys_t* sys = static_cast<vout_display_sys_t*>(vd->sys);
    switch (query)
    {
    case VOUT_DISPLAY_CHANGE_DISPLAY_SIZE:
        return VLC_SUCCESS; // just ignore it
    }
    return VLC_SUCCESS;
}

static void Close(vout_display_t* vd)
{
    vout_display_sys_t* sys = static_cast<vout_display_sys_t*>(vd->sys);

    if (sys->updateOutputCb)
        // tell the host we won't display anything anymore
        sys->updateOutputCb(sys->outside_opaque, nullptr, nullptr);

    if (sys->vctx_in)
        vlc_video_context_Release(sys->vctx_in);

    delete sys;
    msg_Dbg(vd, "DirectX12 display adapter closed");
}

static void PictureUsedInQueue(void *opaque, ID3D12CommandQueue &queue, bool used)
{
    picture_t* pic = static_cast<picture_t*>(opaque);
    picture_sys_d3d12_t *renderSrc = ActiveD3D12PictureSys(pic);
    if (used && renderSrc->pf_wait_display_ready)
        renderSrc->pf_wait_display_ready(*pic->context, &queue);
    if (!used)
        picture_Release(pic);
}

static void PreparePicture(vout_display_t *vd, picture_t *picture, subpicture_t *subpicture,
                           vlc_tick_t date)
{
    VLC_UNUSED(date);
    vout_display_sys_t* sys = static_cast<vout_display_sys_t*>(vd->sys);
    //HRESULT hr;

    picture_sys_d3d12_t *renderSrc = ActiveD3D12PictureSys(picture);
    for (size_t plane = 1; plane < ARRAY_SIZE(renderSrc->resource); plane++)
    {
        assert(renderSrc->resource[plane] == nullptr || renderSrc->resource[0]);
    }
    vlc_d3d12_plane_raw renderInput;
    renderInput.resource = renderSrc->resource[0];
    renderInput.array_slice = renderSrc->slice_index;
    renderInput.used_in_queue = PictureUsedInQueue;
    renderInput.opaque = picture;
    picture_Hold(picture);
    //msg_Dbg(vd, "render slice %d", renderSrc->slice_index);
    sys->selectPlaneCb(sys->outside_opaque, 0, &renderInput);
}

static void Prepare(vout_display_t *vd, picture_t *picture,
                    subpicture_t *subpicture, vlc_tick_t date)
{
    vout_display_sys_t* sys = static_cast<vout_display_sys_t*>(vd->sys);

    if (!sys->startEndRenderingCb || sys->startEndRenderingCb(sys->outside_opaque, true))
    {
        if (sys->sendMetadataCb && picture->format.mastering.max_luminance)
        {
            libvlc_video_frame_hdr10_metadata_t hdr10;
            hdr10.GreenPrimary[0] = picture->format.mastering.primaries[0];
            hdr10.GreenPrimary[1] = picture->format.mastering.primaries[1];
            hdr10.BluePrimary[0]  = picture->format.mastering.primaries[2];
            hdr10.BluePrimary[1]  = picture->format.mastering.primaries[3];
            hdr10.RedPrimary[0]   = picture->format.mastering.primaries[4];
            hdr10.RedPrimary[1]   = picture->format.mastering.primaries[5];
            hdr10.WhitePoint[0]   = picture->format.mastering.white_point[0];
            hdr10.WhitePoint[1]   = picture->format.mastering.white_point[1];
            hdr10.MinMasteringLuminance = picture->format.mastering.min_luminance;
            hdr10.MaxMasteringLuminance = picture->format.mastering.max_luminance;
            hdr10.MaxContentLightLevel = picture->format.lighting.MaxCLL;
            hdr10.MaxFrameAverageLightLevel = picture->format.lighting.MaxFALL;

            sys->sendMetadataCb( sys->outside_opaque, libvlc_video_metadata_frame_hdr10, &hdr10 );
        }

        PreparePicture(vd, picture, subpicture, date);

        if (sys->startEndRenderingCb)
            sys->startEndRenderingCb( sys->outside_opaque, false );
    }
}

static void Display(vout_display_t *vd, picture_t *picture)
{
    vout_display_sys_t* sys = static_cast<vout_display_sys_t*>(vd->sys);
    VLC_UNUSED(picture);

    sys->swapCb(sys->outside_opaque);
}

static void SpuMetadata(vout_display_t* vd, vlc_tick_t start, vlc_tick_t stop,
                        const uint8_t* payload, size_t payloadSize)
{
    auto sys = reinterpret_cast<vout_display_sys_t*>(vd->sys);

    if (sys->spumetadataCb)
        sys->spumetadataCb(sys->spumetadataUserData,
            MS_FROM_VLC_TICK(start), MS_FROM_VLC_TICK(stop),
            payload, payloadSize);
}

static int SetupOutputFormat(vout_display_t* vd, video_format_t* fmt, vlc_video_context* vctx)
{
    vout_display_sys_t* sys = static_cast<vout_display_sys_t*>(vd->sys);

    d3d12_video_context_t* vtcx_sys = GetD3D12ContextPrivate(vctx);
    if (vtcx_sys != nullptr &&
        D3D12_DeviceSupportsFormat(sys->d3d_dev, vtcx_sys->format, D3D12_FORMAT_SUPPORT1_SHADER_LOAD))
    {
        for (const d3d_format_t* output_format = DxgiGetRenderFormatList();
             output_format->name != nullptr; ++output_format)
        {
            if (output_format->formatTexture == vtcx_sys->format &&
                is_d3d12_opaque(output_format->fourcc))
            {
                sys->picQuadFmt = output_format;
                break;
            }
        }
    }
    if (!sys->picQuadFmt)
    {
        msg_Err(vd, "Could not get a suitable texture pixel format");
        return VLC_EGENERIC;
    }

    msg_Dbg(vd, "Using pixel format %s for chroma %4.4s", sys->picQuadFmt->name,
            (char*)&fmt->i_chroma);

    fmt->i_chroma = sys->picQuadFmt->fourcc;
    DxgiFormatMask(sys->picQuadFmt->formatTexture, fmt);
    return VLC_SUCCESS;
}

static int Direct3D12Open(vout_display_t* vd, video_format_t* fmtp, vlc_video_context* vctx)
{
    vout_display_sys_t* sys = static_cast<vout_display_sys_t*>(vd->sys);

    video_format_t fmt;
    video_format_Copy(&fmt, vd->source);
    int err = SetupOutputFormat(vd, &fmt, vctx);
    if (err != VLC_SUCCESS)
    {
        if (!is_d3d12_opaque(vd->source->i_chroma)
#if !VLC_WINSTORE_APP
            && vd->obj.force
#endif
                )
        {
            const vlc_fourcc_t *list = vlc_fourcc_IsYUV(vd->source->i_chroma) ?
                        vlc_fourcc_GetYUVFallback(vd->source->i_chroma) :
                        vlc_fourcc_GetRGBFallback(vd->source->i_chroma);
            for (unsigned i = 0; list[i] != 0; i++) {
                fmt.i_chroma = list[i];
                if (fmt.i_chroma == vd->source->i_chroma)
                    continue;
                err = SetupOutputFormat(vd, &fmt, nullptr);
                if (err == VLC_SUCCESS)
                    break;
            }
        }
        if (err != VLC_SUCCESS)
            return err;
    }

    err = UpdateDisplayFormat(vd, &fmt);
    if (err != VLC_SUCCESS) {
        msg_Err(vd, "Could not update the backbuffer");
        return err;
    }

    video_format_Clean(fmtp);
    *fmtp = fmt;
    sys->vctx_in = vlc_video_context_Hold(vctx);

    return VLC_SUCCESS;
}

static const struct vlc_display_operations ops = {
    Close, Prepare, Display, Control, nullptr, nullptr,
};

static vlc_fourcc_t spu_chromas[] = {
    VLC_CODEC_META_INTERNAL,
    0
};

int OpenDX12Display(vout_display_t *vd, video_format_t *fmtp, vlc_video_context *vctx)
{
    vout_display_sys_t *sys = new (std::nothrow) vout_display_sys_t();
    if (unlikely(sys==nullptr))
        return VLC_ENOMEM;
    vd->sys = sys;

    sys->outside_opaque      = var_InheritAddress( vd, "vout-cb-opaque" );
    sys->updateOutputCb      = (libvlc_video_update_output_cb)var_InheritAddress( vd, "vout-cb-update-output" );
    sys->swapCb              = (libvlc_video_swap_cb)var_InheritAddress( vd, "vout-cb-swap" );
    sys->startEndRenderingCb = (libvlc_video_makeCurrent_cb)var_InheritAddress( vd, "vout-cb-make-current" );
    sys->sendMetadataCb      = (libvlc_video_frameMetadata_cb)var_InheritAddress( vd, "vout-cb-metadata" );
    sys->selectPlaneCb       = (libvlc_video_output_select_plane_cb)var_InheritAddress( vd, "vout-cb-select-plane" );

    sys->spumetadataCb = (libvlc_video_output_spumetadata_cb)var_InheritAddress(vd, "vout-spumetadata-cb");
    sys->spumetadataUserData = var_InheritAddress(vd, "vout-spumetadata-opaque");

    d3d12_decoder_device_t* dev_sys = GetD3D12OpaqueContext( vctx );
    if ( dev_sys == nullptr )
    {
        msg_Err(vd, "Missing external D3D12 device.");
        goto error;
    }
    sys->d3d_dev = &dev_sys->d3d_dev;

    if ( sys->swapCb == nullptr || sys->selectPlaneCb == nullptr || sys->updateOutputCb == nullptr )
    {
        msg_Err(vd, "missing swap/select/update callback");
        goto error;
    }

    if (Direct3D12Open(vd, fmtp, vctx)) {
        msg_Err(vd, "DirectX12 could not be opened");
        goto error;
    }

    vout_window_SetTitle(vd->cfg->window, VOUT_TITLE " (DirectX12 output)");
    msg_Dbg(vd, "DirectX12 display adapter successfully initialized");

    if (sys->spumetadataCb)
    {
        vd->info.can_scale_spu = true;
        vd->info.subpicture_chromas = spu_chromas;
    }
    else
    {
        vd->info.subpicture_chromas = nullptr;
    }

    vd->ops = &ops;
    vd->spumetadata = SpuMetadata;

    return VLC_SUCCESS;

error:
    Close(vd);
    return VLC_EGENERIC;
}
