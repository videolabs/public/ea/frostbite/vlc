/*****************************************************************************
 * mft_d3d12.cpp : Media Foundation Transform audio/video decoder
 *****************************************************************************
 * Copyright (C) 2022 VLC authors and VideoLAN
 *
 * Author: Steve Lhomme <slhomme@videolabs.io>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "mft_d3d12.h"

using Microsoft::WRL::ComPtr;

#if (NTDDI_VERSION < 0x0A00000B) && !defined(_GAMING_XBOX) // NTDDI_WIN10_CO/21H1
// mfd3d12 not available
// not defined in the old Windows SDK
// see https://github.com/microsoft/win32metadata/blob/master/generation/WinSDK/RecompiledIdlHeaders/um/mfd3d12.idl
EXTERN_GUID( MF_D3D12_SYNCHRONIZATION_OBJECT, 0x2a7c8d6a, 0x85a6, 0x494d, 0xa0, 0x46, 0x6, 0xea, 0x1a, 0x13, 0x8f, 0x4b );
// set on the output media type ?
EXTERN_GUID( MF_MT_D3D12_RESOURCE_FLAG_ALLOW_SIMULTANEOUS_ACCESS, 0xa4940b2, 0xcfd6, 0x4738, 0x9d, 0x2, 0x98, 0x11, 0x37, 0x34, 0x1, 0x5a );
#endif // NTDDI_VERSION < NTDDI_WIN10_CO & !_GAMING_XBOX


vlcMFMedia::vlcMFMedia(ComPtr<IMFMediaBuffer> &buffer)
   :mediaBuffer(buffer)
{
    HRESULT hr;
    ComPtr<IMFDXGIBuffer> spDXGIBuffer;
    hr = mediaBuffer.As(&spDXGIBuffer);
    if (SUCCEEDED(hr))
    {
        hr = spDXGIBuffer->GetUnknown(MF_D3D12_SYNCHRONIZATION_OBJECT, IID_PPV_ARGS(&pMFSyncCmd));
        assert(SUCCEEDED(hr));
    }
    resourceReadyForDisplay = CreateEvent(NULL, TRUE, FALSE, NULL);
}

vlcMFMedia::~vlcMFMedia()
{
    CloseHandle(resourceReadyForDisplay);
}

void vlcMFMedia::AddMediaRef()
{
    refcount++;
}

bool vlcMFMedia::ReleaseMedia()
{
    if (--refcount != 0)
        return false;

    WaitForDecoder(nullptr);

    delete this;

    return true;
}

void vlcMFMedia::WaitForDecoder(ID3D12CommandQueue * queue)
{
    vlc::threads::mutex_locker lock {decoder_wait_lock};
    if (waitedForDec)
        return;

    HRESULT hr;
    if (queue)
    {
        hr = pMFSyncCmd->EnqueueResourceReadyWait(queue);
        assert(hr == S_OK);
    }
    else
    {
        // do as with lavc, wait until the buffer is really ready
        hr = pMFSyncCmd->SignalEventOnResourceReady(resourceReadyForDisplay);
        assert(hr == S_OK);

        hr = WaitForSingleObjectEx(resourceReadyForDisplay, INFINITE, TRUE);
        assert(hr == WAIT_OBJECT_0);
    }
    waitedForDec = true;
}


#if 0

NO WAIT:
  * xb1 drops a lot at start
  * pc  drops a bit, eventually CRASHES

GPU WAIT:
  * xb1 smooth
  * pc  drops a lot at start, eventually CRASHES

CPU WAIT out of the decoder (lavc):
  * xb1 lot of drops, slowly catching up
  * pc lot of drops, slowly catching up, eventually CRASHES

CPU WAIT during prepare:
  * xb1 lot of drops, slowly catching up
  * pc smooth

CPU WAIT on destroy:
  * xb1 smooth
  * pc smooth, eventually CRASHES

GPU WAIT + CPU WAIT on destroy:
  * xb1 smooth
  * pc lot of drops, slowly catching up

#endif
