/*****************************************************************************
 * subsvttext.c:
 *****************************************************************************
 * Copyright (C) 2021 VideoLabs, VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_codec.h>
#include <vlc_memstream.h>
#include <modules/codec/webvtt/webvtt.h>
#include <modules/demux/mp4/minibox.h>

#define ATOM_iden VLC_FOURCC('i', 'd', 'e', 'n')
#define ATOM_payl VLC_FOURCC('p', 'a', 'y', 'l')
#define ATOM_sttg VLC_FOURCC('s', 't', 't', 'g')
#define ATOM_vttc VLC_FOURCC('v', 't', 't', 'c')
#define ATOM_vtte VLC_FOURCC('v', 't', 't', 'e')
#define ATOM_vttx VLC_FOURCC('v', 't', 't', 'x')

static void OutputTime(struct vlc_memstream *ms, mtime_t i_time)
{
    mtime_t secs = i_time / CLOCK_FREQ;
    vlc_memstream_printf( ms, "%u:%02u:%02u.%03u",
                          (unsigned) (secs / 3600),
                          (unsigned) (secs % 3600 / 60),
                          (unsigned) (secs % 60),
                          (unsigned) (i_time % CLOCK_FREQ) / 1000 );
}

static block_t * UnpackISOBMFF( const block_t *p_block )
{
    struct vlc_memstream ms;
    if( vlc_memstream_open( &ms ) )
        return NULL;

    vlc_memstream_printf(&ms, "WEBVTT\n\n");

    mp4_box_iterator_t it;
    mp4_box_iterator_Init( &it, p_block->p_buffer, p_block->i_buffer );
    while( mp4_box_iterator_Next( &it ) )
    {
        if( it.i_type == ATOM_vttc || it.i_type == ATOM_vttx )
        {
            char *psz_iden = NULL;
            char *psz_sttg = NULL;
            char *psz_payl = NULL;

            mp4_box_iterator_t vtcc;
            mp4_box_iterator_Init( &vtcc, it.p_payload, it.i_payload );
            while( mp4_box_iterator_Next( &vtcc ) )
            {
                switch( vtcc.i_type )
                {
                    case ATOM_iden:
                        if(!psz_iden)
                            psz_iden = strndup( (const char*)vtcc.p_payload, vtcc.i_payload );
                        break;
                    case ATOM_sttg:
                        if(psz_sttg)
                        {
                            char *dup = strndup( (const char*)vtcc.p_payload, vtcc.i_payload );
                            if( dup )
                            {
                                char *psz;
                                if( asprintf( &psz, "%s %s", psz_sttg, dup ) >= 0 )
                                {
                                    free( psz_sttg );
                                    psz_sttg = psz;
                                }
                                free( dup );
                            }
                        }
                        else psz_sttg = strndup( (const char*)vtcc.p_payload, vtcc.i_payload );
                        break;
                    case ATOM_payl:
                        if(!psz_payl)
                            psz_payl = strndup( (const char*)vtcc.p_payload, vtcc.i_payload );
                        break;
                }
            }

            if( psz_iden )
                vlc_memstream_printf( &ms, "%s\n", psz_iden );

            OutputTime( &ms, p_block->i_dts - VLC_TICK_0 );
            vlc_memstream_printf( &ms, " --> " );
            OutputTime( &ms, p_block->i_dts - VLC_TICK_0 + p_block->i_length );

            if( psz_sttg )
                vlc_memstream_printf( &ms, " %s\n", psz_sttg );
            else
                vlc_memstream_putc( &ms, '\n' );

            vlc_memstream_printf( &ms, "%s\n\n", psz_payl );

            free( psz_iden );
            free( psz_sttg );
            free( psz_payl );
        }
    }

    if( vlc_memstream_close( &ms ) )
        return NULL;

    return block_heap_Alloc( ms.ptr, ms.length );
}

static size_t vlc_memstream_writeQISONescaped(struct vlc_memstream *ms,
                                              const char *s, size_t len)
{
    if(!strchr(s, '"'))
        return vlc_memstream_write(ms, s, len);

    size_t written = 0;
    for(size_t i=0; i<len; i++)
    {
        if(s[i] == '"')
            vlc_memstream_putc(ms, '\\');
        if(s[i] == '\\')
            vlc_memstream_putc(ms, '\\');
        if(s[i] == '\n')
        {
            vlc_memstream_putc(ms, '\\');
            vlc_memstream_putc(ms, 'n');
            continue;
        }
        vlc_memstream_putc(ms, s[i]);
    }
    return written;
}

static block_t * UnpackISOBMFFToQISON( const block_t *p_block )
{
    struct vlc_memstream ms;
    if( vlc_memstream_open( &ms ) )
        return NULL;

    mp4_box_iterator_t it;
    mp4_box_iterator_Init( &it, p_block->p_buffer, p_block->i_buffer );
    while( mp4_box_iterator_Next( &it ) )
    {
        if( it.i_type == ATOM_vttc || it.i_type == ATOM_vttx )
        {
            vlc_memstream_puts(&ms, "\"start\": \"");
            OutputTime( &ms, p_block->i_dts - VLC_TICK_0 );
            vlc_memstream_puts(&ms, "\"\n");
            vlc_memstream_puts(&ms, "\"stop\": \"");
            OutputTime( &ms, p_block->i_dts - VLC_TICK_0 + p_block->i_length );
            vlc_memstream_puts(&ms, "\"\n");

            mp4_box_iterator_t vtcc;
            mp4_box_iterator_Init( &vtcc, it.p_payload, it.i_payload );
            while( mp4_box_iterator_Next( &vtcc ) )
            {
                switch( vtcc.i_type )
                {
                    case ATOM_payl:
                        vlc_memstream_puts(&ms, "\"text\": \"");
                        vlc_memstream_writeQISONescaped( &ms, (const char*)vtcc.p_payload, vtcc.i_payload );
                        vlc_memstream_puts(&ms, "\"\n");
                        break;
                    default:
                        break;
                }
            }
        }
    }

    if( vlc_memstream_close( &ms ) )
        return NULL;

    return block_heap_Alloc( ms.ptr, ms.length );
}

static int DecodeBlock( decoder_t *p_dec, block_t *p_block )
{
    if( p_block )
        decoder_QueueExternalDecoderSub( p_dec, p_block );
    return VLCDEC_SUCCESS;
}

static int DecodeBlockWEBVTT( decoder_t *p_dec, block_t *p_block )
{
    if( p_block )
    {
        block_t *p_text = UnpackISOBMFF( p_block );
        if( p_text )
        {
            block_CopyProperties( p_text, p_block );
            decoder_QueueExternalDecoderSub( p_dec, p_text );
        }
        block_Release( p_block );
    }
    return VLCDEC_SUCCESS;
}

static int DecodeBlockQISON( decoder_t *p_dec, block_t *p_block )
{
    if( p_block )
    {
        block_t *p_text = UnpackISOBMFFToQISON( p_block );
        if( p_text )
        {
            block_CopyProperties( p_text, p_block );
            decoder_QueueExternalDecoderSub( p_dec, p_text );
        }
        block_Release( p_block );
    }
    return VLCDEC_SUCCESS;
}

void subsvttext_CloseDecoder(vlc_object_t* p_this)
{
    decoder_t *p_dec = (decoder_t *)p_this;
    (void) p_dec;
}

enum
{
    OUTPUT_FORMAT_WEBVTT = 0,
    OUTPUT_FORMAT_ISOBMFF,
    OUTPUT_FORMAT_QISON,
};

#define MODULE_PREFIX "subsvttext-"

int subsvttext_OpenDecoder(vlc_object_t* p_this)
{
    decoder_t *p_dec = (decoder_t*)p_this;

    if( !p_dec->cbs->spu.queue_ext )
        return VLC_EGENERIC;

    if( p_dec->fmt_in.i_codec != VLC_CODEC_WEBVTT )
        return VLC_EGENERIC;

    switch(var_InheritInteger(p_this, MODULE_PREFIX"format"))
    {
        case OUTPUT_FORMAT_QISON:
            p_dec->pf_decode = DecodeBlockQISON;
            break;
        case OUTPUT_FORMAT_ISOBMFF:
            p_dec->pf_decode = DecodeBlock;
            break;
        default:
            p_dec->pf_decode = DecodeBlockWEBVTT;
            break;
    }

    es_format_Copy( &p_dec->fmt_out, &p_dec->fmt_in );

    return VLC_SUCCESS;
}
