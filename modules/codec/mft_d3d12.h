/*****************************************************************************
 * mft_d3d12.h : Media Foundation Transform audio/video decoder
 *****************************************************************************
 * Copyright (C) 2022 VLC authors and VideoLAN
 *
 * Author: Steve Lhomme <slhomme@videolabs.io>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef VLC_MFT_D3D12_H
#define VLC_MFT_D3D12_H

#include <vlc_common.h>
#include <vlc_cxx_helpers.hpp>

#include "../hw/d3d12/d3d12_fmt.h"

#include <mfapi.h>
#include <mftransform.h>
#include <mferror.h>
#include <mfobjects.h>
#include <codecapi.h>
#include <wmcodecdsp.h>

#include <mfidl.h>
#include <mfreadwrite.h>

#if (NTDDI_VERSION >= 0x0A00000B) || defined(_GAMING_XBOX)  // NTDDI_WIN10_CO/21H1
#include <mfd3d12.h>
#else //NTDDI_VERSION < NTDDI_WIN10_CO & !_GAMING_XBOX
// not defined in the old Windows SDK
MIDL_INTERFACE("09D0F835-92FF-4E53-8EFA-40FAA551F233")
IMFD3D12SynchronizationObjectCommands : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE EnqueueResourceReady(ID3D12CommandQueue *pProducerCommandQueue) = 0;
    virtual HRESULT STDMETHODCALLTYPE EnqueueResourceReadyWait(ID3D12CommandQueue *pConsumerCommandQueue) = 0;
    virtual HRESULT STDMETHODCALLTYPE SignalEventOnResourceReady(HANDLE hEvent) = 0;
    virtual HRESULT STDMETHODCALLTYPE EnqueueResourceRelease(ID3D12CommandQueue *pConsumerCommandQueue) = 0;
};

MIDL_INTERFACE("802302B0-82DE-45E1-B421-F19EE5BDAF23")
IMFD3D12SynchronizationObject : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE SignalEventOnFinalResourceRelease(HANDLE hEvent) = 0;
    virtual HRESULT STDMETHODCALLTYPE Reset(void) = 0;
};
#endif // NTDDI_VERSION < NTDDI_WIN10_CO & !_GAMING_XBOX


#include <atomic>

#include <wrl/client.h>

class vlcMFMedia
{
public:
    vlcMFMedia( Microsoft::WRL::ComPtr<IMFMediaBuffer> &);
    ~vlcMFMedia();
    void AddMediaRef();
    bool ReleaseMedia();

    void WaitForDecoder(ID3D12CommandQueue * queue);

private:
    Microsoft::WRL::ComPtr<IMFMediaBuffer> mediaBuffer;

    std::atomic<size_t>  refcount{1};
    HANDLE resourceReadyForDisplay = INVALID_HANDLE_VALUE;

    vlc::threads::mutex  decoder_wait_lock;
    bool                 waitedForDec = false;
    Microsoft::WRL::ComPtr<IMFD3D12SynchronizationObjectCommands> pMFSyncCmd;
};

#endif // VLC_MFT_D3D12_H
