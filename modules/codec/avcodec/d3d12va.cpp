/*****************************************************************************
 * d3d11va.c: Direct3D11 Video Acceleration decoder
 *****************************************************************************
 * Copyright © 2009 Geoffroy Couprie
 * Copyright © 2009 Laurent Aimar
 * Copyright © 2015 Steve Lhomme
 * Copyright © 2015 VideoLabs
 *
 * Authors: Geoffroy Couprie <geal@videolan.org>
 *          Laurent Aimar <fenrir _AT_ videolan _DOT_ org>
 *          Steve Lhomme <robux4@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/**
  * See https://msdn.microsoft.com/en-us/library/windows/desktop/hh162912%28v=vs.85%29.aspx
  **/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

# undef WINAPI_FAMILY
# define WINAPI_FAMILY WINAPI_FAMILY_DESKTOP_APP

#include <assert.h>

#include <vlc_common.h>
#include <vlc_picture.h>
#include <vlc_plugin.h>
#include <vlc_charset.h>
#include <vlc_codec.h>

#include "../../hw/d3d12/d3d12_fmt.h"

#include <libavcodec/d3d12va.h>

struct d3d12va_pic_context
{
    struct d3d12_pic_context  ctx;
    struct vlc_va_surface_t   *va_surface;
};

#define D3D12VA_PICCONTEXT_FROM_PICCTX(pic_ctx)  \
    container_of((pic_ctx), d3d12va_pic_context, ctx.s)

#include "directx_va.h"

EXTERN_SYMBOL int OpenD3D12va(vlc_va_t *, AVCodecContext *, enum AVPixelFormat hwfmt, const AVPixFmtDescriptor *,
                const es_format_t *, vlc_decoder_device *, video_format_t *, vlc_video_context **);

#ifndef FB_VLC_MODULES_AGGREGATE
vlc_module_begin()
    set_description(N_("Direct3D12 Video Acceleration"))
    set_category(CAT_INPUT)
    set_subcategory(SUBCAT_INPUT_VCODEC)
    set_va_callback(OpenD3D12va, 110)
vlc_module_end()
#endif

DEFINE_GUID(DXVA2_NoEncrypt,                        0x1b81bed0, 0xa0c7, 0x11d3, 0xb9, 0x84, 0x00, 0xc0, 0x4f, 0x2e, 0x73, 0xc5);

struct vlc_va_sys_t
{
    d3d12_device_t               *d3d_dev;

    vlc_video_context            *vctx;

    const d3d_format_t           *render_fmt;

    /* Video decoder */
    ID3D12VideoDevice              *d3ddec;
    const directx_va_mode_t        *selected_decoder;

    /* avcodec internals */
    struct AVD3D12VAContext       hw;

    /* pool */
    va_pool_t                     *va_pool;
    ID3D12Resource                *p_texture;

    ID3D12Resource                *ref_table[MAX_SURFACE_COUNT];
    UINT                          ref_index[MAX_SURFACE_COUNT];
#if !(defined(_GAMING_XBOX) || defined(_XBOX_ONE))
    ID3D12VideoDecoderHeap* ref_heaps[MAX_SURFACE_COUNT];
#endif
};

/* */
static int D3dCreateDevice(vlc_va_t *);

static int DxGetInputList(vlc_va_t *, input_list_t *);
static int DxSetupOutput(vlc_va_t *, const directx_va_mode_t *, const video_format_t *);

static int DxCreateDecoderSurfaces(vlc_va_t *, int codec_id,
                                   const video_format_t *fmt, size_t surface_count);
static void DxDestroySurfaces(void *);

static void SetupAVCodecContext(void *opaque, AVCodecContext *avctx)
{
    vlc_va_sys_t *sys = static_cast<vlc_va_sys_t *>(opaque);
    sys->hw.workaround = sys->selected_decoder->workaround;
    sys->hw.gpu_node_mask = sys->d3d_dev->gpuNodeMask;
    avctx->hwaccel_context = &sys->hw;
}

static void d3d12va_pic_context_destroy(picture_context_t *ctx)
{
    d3d12va_pic_context *pic_ctx = D3D12VA_PICCONTEXT_FROM_PICCTX(ctx);
    vlc_va_surface_t *va_surface = pic_ctx->va_surface;
    static_assert(offsetof(d3d12va_pic_context, ctx.s) == 0,
        "Cast assumption failure");
    d3d12_pic_context_destroy(ctx);
    va_surface_Release(va_surface);
}

static picture_context_t *d3d12va_pic_context_copy(picture_context_t *ctx)
{
    d3d12va_pic_context *src_ctx = D3D12VA_PICCONTEXT_FROM_PICCTX(ctx);
    d3d12va_pic_context *pic_ctx = static_cast<d3d12va_pic_context *>(malloc(sizeof(*pic_ctx)));
    if (unlikely(pic_ctx==nullptr))
        return nullptr;
    *pic_ctx = *src_ctx;
    vlc_video_context_Hold(pic_ctx->ctx.s.vctx);
    va_surface_AddRef(pic_ctx->va_surface);

    // pic_ctx->ctx.picsys.slice_index = src_ctx->ctx.picsys.slice_index;
    // pic_ctx->ctx.picsys.array_size = src_ctx->ctx.picsys.array_size;
    for (int i=0;i<DXGI_MAX_SHADER_VIEW; i++)
        pic_ctx->ctx.picsys.resource[i]  = src_ctx->ctx.picsys.resource[i];
    AcquireD3D12PictureSys(&pic_ctx->ctx.picsys);

    return &pic_ctx->ctx.s;
}

static d3d12va_pic_context *CreatePicContext(
                                                  ID3D12Resource *p_resource,
                                                  UINT slice,
                                                  const DXGI_FORMAT resourceFormat[DXGI_MAX_SHADER_VIEW],
                                                  vlc_video_context *vctx)
{
    d3d12va_pic_context *pic_ctx = static_cast<d3d12va_pic_context *>(calloc(1, sizeof(*pic_ctx)));
    if (unlikely(pic_ctx==nullptr))
        return nullptr;
    pic_ctx->ctx.s = {
        d3d12va_pic_context_destroy, d3d12va_pic_context_copy,
        vlc_video_context_Hold(vctx),
    };

    pic_ctx->ctx.picsys.slice_index = slice;
    for (int i=0;i<DXGI_MAX_SHADER_VIEW; i++)
    {
        if (!resourceFormat[i])
            pic_ctx->ctx.picsys.resource[i] = nullptr;
        else
        {
            pic_ctx->ctx.picsys.resource[i] = p_resource;
            pic_ctx->ctx.picsys.resource_plane[i] = resourceFormat[i]==DXGI_FORMAT_R8G8_UNORM ? 1 : i;
        }
    }
    AcquireD3D12PictureSys(&pic_ctx->ctx.picsys);

    return pic_ctx;
}

static picture_context_t* NewSurfacePicContext(vlc_va_t *va, vlc_va_surface_t *va_surface)
{
    vlc_va_sys_t *sys = static_cast<vlc_va_sys_t*>(va->sys);
    d3d12va_pic_context *pic_ctx = CreatePicContext(
                                                  sys->p_texture,
                                                  va_surface_GetIndex(va_surface),
                                                  sys->render_fmt->resourceFormat,
                                                  sys->vctx);
    if (unlikely(pic_ctx==nullptr))
        return nullptr;
    pic_ctx->va_surface = va_surface;
    return &pic_ctx->ctx.s;
}

static int Get(vlc_va_t* va, picture_t* pic, AVCodecContext* ctx, AVFrame* frame)
{
    vlc_va_sys_t* sys = static_cast<vlc_va_sys_t*>(va->sys);
    vlc_va_surface_t* va_surface = va_pool_Get(sys->va_pool);
    if (unlikely(va_surface == nullptr))
        return VLC_ENOENT;
    pic->context = NewSurfacePicContext(va, va_surface);
    if (unlikely(pic->context == nullptr))
    {
        va_surface_Release(va_surface);
        return VLC_ENOMEM;
    }
    frame->data[3] = (uint8_t*)(uintptr_t)va_surface_GetIndex(va_surface);
    return VLC_SUCCESS;
}

static void Close(vlc_va_t *va)
{
    vlc_va_sys_t* sys = static_cast<vlc_va_sys_t*>(va->sys);

    if (sys->vctx)
        vlc_video_context_Release(sys->vctx);

    if (sys->va_pool)
        va_pool_Close(sys->va_pool);
}

static const struct vlc_va_operations ops = { Get, Close, };

int OpenD3D12va(vlc_va_t *va, AVCodecContext *ctx, enum AVPixelFormat hwfmt, const AVPixFmtDescriptor *desc,
                const es_format_t *fmt_in, vlc_decoder_device *dec_device,
                video_format_t *fmt_out, vlc_video_context **vtcx_out)
{
    int err = VLC_EGENERIC;

    ctx->hwaccel_context = nullptr;

    if ( hwfmt != AV_PIX_FMT_D3D12_VLD )
        return VLC_EGENERIC;

    d3d12_decoder_device_t *devsys = GetD3D12OpaqueDevice( dec_device );
    if ( devsys == nullptr )
        return VLC_EGENERIC;

    vlc_va_sys_t *sys = static_cast<vlc_va_sys_t *>(calloc(1, sizeof (*sys)));
    if (unlikely(sys == nullptr))
        return VLC_ENOMEM;

    video_format_t final_fmt = *fmt_out;
    va->sys = sys;

    sys->render_fmt = nullptr;
    sys->d3d_dev = &devsys->d3d_dev;

    struct va_pool_cfg pool_cfg = {
        D3dCreateDevice,
        DxDestroySurfaces,
        DxCreateDecoderSurfaces,
        SetupAVCodecContext,
        sys,
    };

    sys->va_pool = va_pool_Create(va, &pool_cfg);
    if (sys->va_pool == nullptr)
    {
        err = VLC_EGENERIC;
        goto error;
    }

    static const directx_sys_t dx_sys = { DxGetInputList, DxSetupOutput };
    sys->selected_decoder = directx_va_Setup(va, &dx_sys, ctx, desc, fmt_in, isXboxHardware(sys->d3d_dev),
                                             &final_fmt, &sys->hw.surfaces.NumTexture2Ds);
    if (sys->selected_decoder == nullptr)
    {
        err = VLC_EGENERIC;
        goto error;
    }

    final_fmt.i_chroma = sys->render_fmt->fourcc;
    err = va_pool_SetupDecoder(va, sys->va_pool, ctx, &final_fmt, sys->hw.surfaces.NumTexture2Ds);
    if (err != VLC_SUCCESS)
        goto error;

#ifdef TODO_D3D12
    msg_Info(va, "Using D3D12VA (%ls, vendor %x(%s), device %x, revision %x)",
                sys->d3d_dev->adapterDesc.Description,
                sys->d3d_dev->adapterDesc.VendorId, DxgiVendorStr(sys->d3d_dev->adapterDesc.VendorId),
                sys->d3d_dev->adapterDesc.DeviceId, sys->d3d_dev->adapterDesc.Revision);
#endif

    sys->vctx = D3D12CreateVideoContext(dec_device, sys->render_fmt->formatTexture);
    if (sys->vctx == nullptr)
    {
        msg_Dbg(va, "no video context");
        err = VLC_EGENERIC;
        goto error;
    }

    va->ops = &ops;
    *fmt_out = final_fmt;
    *vtcx_out = sys->vctx;
    return VLC_SUCCESS;

error:
    Close(va);
    return err;
}

/**
 * It creates a Direct3D device usable for decoding
 */
static int D3dCreateDevice(vlc_va_t *va)
{
    vlc_va_sys_t* sys = static_cast<vlc_va_sys_t*>(va->sys);
    HRESULT hr;

    hr = sys->d3d_dev->d3ddevice.CopyTo(IID_GRAPHICS_PPV_ARGS(&sys->d3ddec));
    if (FAILED(hr)) {
       msg_Err(va, "Could not create ID3D12VideoDevice Interface. (hr=0x%lX)", hr);
       return VLC_EGENERIC;
    }

    return VLC_SUCCESS;
}

static void ReleaseInputList(input_list_t *p_list)
{
    free(p_list->list);
}

static int DxGetInputList(vlc_va_t *va, input_list_t *p_list)
{
    vlc_va_sys_t* sys = static_cast<vlc_va_sys_t*>(va->sys);
    HRESULT hr;

    D3D12_FEATURE_DATA_VIDEO_DECODE_PROFILE_COUNT profiles = {};
    profiles.NodeIndex = sys->d3d_dev->gpuNodeIndex;
    hr = sys->d3ddec->CheckFeatureSupport(D3D12_FEATURE_VIDEO_DECODE_PROFILE_COUNT, &profiles, sizeof(profiles));
    if (FAILED(hr))
    {
        msg_Err(va, "Failed to get the number of profiles. (hr=0x%lX)", hr);
        return VLC_EGENERIC;
    }
    p_list->count = profiles.ProfileCount;
    p_list->list = static_cast<GUID*>(calloc(p_list->count, sizeof(*p_list->list)));
    if (unlikely(p_list->list == nullptr)) {
        return VLC_ENOMEM;
    }
    p_list->pf_release = ReleaseInputList;
    D3D12_FEATURE_DATA_VIDEO_DECODE_PROFILES profile;
    profile.NodeIndex = sys->d3d_dev->gpuNodeIndex;
    profile.ProfileCount = profiles.ProfileCount;
    profile.pProfiles = p_list->list;
    hr = sys->d3ddec->CheckFeatureSupport(D3D12_FEATURE_VIDEO_DECODE_PROFILES, &profile, sizeof(profile));
    if (FAILED(hr))
    {
        msg_Err(va, "Failed to get the decoder profiles failed. (hr=0x%lX)", hr);
        ReleaseInputList(p_list);
        return VLC_EGENERIC;
    }

    return VLC_SUCCESS;
}

static const d3d_format_t *D3D12_FindDXGIFormat(DXGI_FORMAT dxgi)
{
    for (const d3d_format_t *output_format = DxgiGetRenderFormatList();
         output_format->name != nullptr; ++output_format)
    {
        if (output_format->formatTexture == dxgi &&
                is_d3d12_opaque(output_format->fourcc))
        {
            return output_format;
        }
    }
    return nullptr;
}

static int DxSetupOutput(vlc_va_t *va, const directx_va_mode_t *mode, const video_format_t *fmt)
{
    vlc_va_sys_t* sys = static_cast<vlc_va_sys_t*>(va->sys);
    HRESULT hr;

#ifdef TODO_D3D12
#ifndef NDEBUG
    BOOL bSupported = false;
    for (int format = 0; format < 188; format++) {
        hr = sys->d3ddec->CheckVideoDecoderFormat(mode->guid, format, &bSupported);
        if (SUCCEEDED(hr) && bSupported)
            msg_Dbg(va, "format %s is supported for output", DxgiFormatToStr(format));
    }
#endif

    if (!directx_va_canUseDecoder(va, sys->d3d_dev->adapterDesc.VendorId, sys->d3d_dev->adapterDesc.DeviceId,
                                  mode->guid, sys->d3d_dev->WDDM.build))
    {
        msg_Warn(va, "GPU blocklisted for %s codec", mode->name);
        return VLC_EGENERIC;
    }
#endif

    const d3d_format_t *processorInput[4];
    int idx = 0;
    const d3d_format_t *decoder_format;
    UINT supportFlags = D3D12_FORMAT_SUPPORT1_DECODER_OUTPUT | D3D12_FORMAT_SUPPORT1_SHADER_LOAD;
    decoder_format = FindD3D12Format( sys->d3d_dev, 0, DXGI_RGB_FORMAT|DXGI_YUV_FORMAT,
                                      mode->bit_depth, mode->log2_chroma_h+1, mode->log2_chroma_w+1,
                                      DXGI_CHROMA_GPU, supportFlags );
    if (decoder_format == nullptr)
        decoder_format = FindD3D12Format( sys->d3d_dev, 0, DXGI_RGB_FORMAT|DXGI_YUV_FORMAT,
                                        mode->bit_depth, 0, 0, DXGI_CHROMA_GPU, supportFlags );
    if (decoder_format == nullptr && mode->bit_depth > 10)
        decoder_format = FindD3D12Format( sys->d3d_dev, 0, DXGI_RGB_FORMAT|DXGI_YUV_FORMAT,
                                        10, 0, 0, DXGI_CHROMA_GPU, supportFlags );
    if (decoder_format == nullptr)
        decoder_format = FindD3D12Format( sys->d3d_dev, 0, DXGI_RGB_FORMAT|DXGI_YUV_FORMAT,
                                        0, 0, 0, DXGI_CHROMA_GPU, supportFlags );
    if (decoder_format != nullptr)
    {
        msg_Dbg(va, "favor decoder format %s", decoder_format->name);
        processorInput[idx++] = decoder_format;
    }

    if (decoder_format == nullptr || decoder_format->formatTexture != DXGI_FORMAT_NV12)
        processorInput[idx++] = D3D12_FindDXGIFormat(DXGI_FORMAT_NV12);
    processorInput[idx++] = D3D12_FindDXGIFormat(DXGI_FORMAT_420_OPAQUE);
    processorInput[idx++] = nullptr;

    D3D12_FEATURE_DATA_VIDEO_DECODE_FORMAT_COUNT decode_formats = { };
    decode_formats.NodeIndex = sys->d3d_dev->gpuNodeIndex;
    decode_formats.Configuration.DecodeProfile = *mode->guid;
    hr = sys->d3ddec->CheckFeatureSupport(D3D12_FEATURE_VIDEO_DECODE_FORMAT_COUNT, &decode_formats, sizeof(decode_formats));
    if (FAILED(hr) || decode_formats.FormatCount==0)
    {
        msg_Dbg(va, "Can't get a decoder output for decoder %s.", mode->name);
        return VLC_EGENERIC;
    }
    DXGI_FORMAT supported_formats[16]; // decoders usually don't support that many formats
    D3D12_FEATURE_DATA_VIDEO_DECODE_FORMATS formats = { };
    formats.NodeIndex      = sys->d3d_dev->gpuNodeIndex;
    formats.Configuration  = decode_formats.Configuration;
    formats.FormatCount    = __MIN(decode_formats.FormatCount, (UINT)ARRAY_SIZE(supported_formats));
    formats.pOutputFormats = supported_formats;
    hr = sys->d3ddec->CheckFeatureSupport(D3D12_FEATURE_VIDEO_DECODE_FORMATS, &formats, sizeof(formats));
    if (FAILED(hr))
    {
        msg_Dbg(va, "Can't get a decoded formats for decoder %s.", mode->name);
        return VLC_EGENERIC;
    }

    /* */
    for (idx = 0; processorInput[idx] != nullptr; ++idx)
    {
        BOOL is_supported = false;
        for (size_t f=0; f<decode_formats.FormatCount; f++)
        {
            if (supported_formats[f] == processorInput[idx]->formatTexture)
            {
                D3D12_FEATURE_DATA_VIDEO_DECODE_SUPPORT decode_support = { };
                decode_support.Configuration.DecodeProfile = *mode->guid;
                decode_support.Configuration.BitstreamEncryption = D3D12_BITSTREAM_ENCRYPTION_TYPE_NONE;
                decode_support.Configuration.InterlaceType = D3D12_VIDEO_FRAME_CODED_INTERLACE_TYPE_NONE;
                decode_support.DecodeFormat = processorInput[idx]->formatTexture;
                decode_support.Width = fmt->i_width;
                decode_support.Height = fmt->i_height;
                if (fmt->i_frame_rate && fmt->i_frame_rate_base)
                {
                    decode_support.FrameRate =
                        { /*.Numerator =*/ fmt->i_frame_rate, /*.Denominator =*/ fmt->i_frame_rate_base };
                }
                hr = sys->d3ddec->CheckFeatureSupport(D3D12_FEATURE_VIDEO_DECODE_SUPPORT, &decode_support, sizeof(decode_support));
                if (SUCCEEDED(hr))
                {
                    if (decode_support.SupportFlags & D3D12_VIDEO_DECODE_SUPPORT_FLAG_SUPPORTED)
                    {
                        is_supported = true;
                        break;
                    }
                }
            }
        }
        if (SUCCEEDED(hr) && is_supported)
            msg_Dbg(va, "%s output is supported for decoder %s.", DxgiFormatToStr(processorInput[idx]->formatTexture), mode->name);
        else
        {
            msg_Dbg(va, "Can't get a decoder output format %s for decoder %s.", DxgiFormatToStr(processorInput[idx]->formatTexture), mode->name);
            continue;
        }
#ifdef TODO_D3D12

       // check if we can create render texture of that format
       // check the decoder can output to that format
       if ( !D3D11_DeviceSupportsFormat(sys->d3d_dev, processorInput[idx]->formatTexture,
                                  D3D12_FORMAT_SUPPORT1_SHADER_LOAD) )
       {
           if ( !D3D11_DeviceSupportsFormat(sys->d3d_dev, processorInput[idx]->formatTexture,
                                      D3D12_FORMAT_SUPPORT_VIDEO_PROCESSOR_INPUT) )
           {
               msg_Dbg(va, "Format %s needs a processor but is not available",
                       DxgiFormatToStr(processorInput[idx]->formatTexture));
               continue;
           }
        }

        D3D12_VIDEO_DECODER_DESC decoderDesc;
        ZeroMemory(&decoderDesc, sizeof(decoderDesc));
        decoderDesc.Guid = *mode->guid;
        decoderDesc.SampleWidth = fmt->i_width;
        decoderDesc.SampleHeight = fmt->i_height;
        decoderDesc.OutputFormat = processorInput[idx]->formatTexture;

        UINT cfg_count = 0;
        hr = sys->d3ddec->GetVideoDecoderConfigCount(&decoderDesc, &cfg_count );
        if (FAILED(hr))
        {
            msg_Err( va, "Failed to get configuration for decoder %s. (hr=0x%lX)", mode->name, hr );
            continue;
        }
        if (cfg_count == 0) {
            msg_Err( va, "No decoder configuration possible for %s %dx%d",
                     DxgiFormatToStr(decoderDesc.OutputFormat),
                     decoderDesc.SampleWidth, decoderDesc.SampleHeight );
            continue;
        }
#endif

        msg_Dbg(va, "Using output format %s for decoder %s", DxgiFormatToStr(processorInput[idx]->formatTexture), mode->name);
        sys->render_fmt = processorInput[idx];
        return VLC_SUCCESS;
    }

    msg_Dbg(va, "Output format from picture source not supported.");
    return VLC_EGENERIC;
}

#ifdef TODO_D3D12
static bool CanUseDecoderPadding(const vlc_va_sys_t* sys)
{
    /* Qualcomm hardware has issues with textures and pixels that should not be
    * part of the decoded area */
    return sys->d3d_dev->adapterDesc.VendorId != GPU_MANUFACTURER_QUALCOMM;
}
#endif

/**
 * It creates a Direct3D12 decoder using the given video format
 */
static int DxCreateDecoderSurfaces(vlc_va_t *va, int codec_id,
                                   const video_format_t *fmt, size_t surface_count)
{
    vlc_va_sys_t* sys = static_cast<vlc_va_sys_t*>(va->sys);
    HRESULT hr;

#if VLC_WINSTORE_APP
    /* On the Xbox 1/S, any decoding of H264 with one dimension over 2304
     * crashes totally the device */
    if (codec_id == AV_CODEC_ID_H264 &&
        (fmt->i_width > 2304 || fmt->i_height > 2304) &&
        isXboxHardware(sys->d3d_dev))
    {
        msg_Warn(va, "%dx%d resolution not supported by your hardware", fmt->i_width, fmt->i_height);
        return VLC_EGENERIC;
    }
#else
    VLC_UNUSED(codec_id);
#endif

    D3D12_RESOURCE_DESC m_textureDesc;
    m_textureDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    m_textureDesc.Alignment = 0;
    m_textureDesc.Width = fmt->i_width;
    m_textureDesc.Height = fmt->i_height;
    m_textureDesc.DepthOrArraySize = surface_count;
    m_textureDesc.MipLevels = 1;
    m_textureDesc.Format = sys->render_fmt->formatTexture;
    m_textureDesc.SampleDesc.Count = 1;
    m_textureDesc.SampleDesc.Quality = 0;
    m_textureDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    m_textureDesc.Flags = D3D12_RESOURCE_FLAG_NONE;
    m_textureDesc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_SIMULTANEOUS_ACCESS;

    D3D12_HEAP_PROPERTIES m_textureProp;
    m_textureProp.Type = D3D12_HEAP_TYPE_DEFAULT;
    m_textureProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    m_textureProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    m_textureProp.CreationNodeMask = sys->d3d_dev->gpuNodeMask;
    m_textureProp.VisibleNodeMask = m_textureProp.CreationNodeMask;

    hr = sys->d3d_dev->d3ddevice->CreateCommittedResource(&m_textureProp, D3D12_HEAP_FLAG_NONE,
                                               &m_textureDesc, D3D12_RESOURCE_STATE_COMMON, nullptr,
                                               IID_GRAPHICS_PPV_ARGS(&sys->p_texture));
    if (FAILED(hr)) {
        msg_Err(va, "CreateResource of %zu slices failed. (hr=0x%lX)", surface_count, hr);
        return VLC_EGENERIC;
    }
    D3D12_ObjectSetName(sys->p_texture, "decoder texture");

    unsigned surface_idx;
    for (surface_idx = 0; surface_idx < surface_count; surface_idx++) {
        sys->ref_table[surface_idx] = sys->p_texture;
        sys->ref_index[surface_idx] = surface_idx;
#if !(defined(_GAMING_XBOX) || defined(_XBOX_ONE))
        sys->ref_heaps[surface_idx] = nullptr;
#endif
    }
    sys->hw.surfaces = {};
    sys->hw.surfaces.NumTexture2Ds = (UINT)surface_count;
    sys->hw.surfaces.ppTexture2Ds = sys->ref_table;
    sys->hw.surfaces.pSubresources = sys->ref_index;
// #if !(defined(_GAMING_XBOX) || defined(_XBOX_ONE))
//     sys->hw.surfaces.ppHeaps = sys->ref_heaps;
// #endif

    msg_Dbg(va, "ID3D12Resource decoder succeed with %zu surfaces (%dx%d)",
            surface_count, fmt->i_width, fmt->i_height);
#ifdef TODO_D3D12

    D3D12_VIDEO_DECODER_DESC decoderDesc;
    ZeroMemory(&decoderDesc, sizeof(decoderDesc));
    decoderDesc.Guid = *sys->selected_decoder->guid;
    decoderDesc.SampleWidth = fmt->i_width;
    decoderDesc.SampleHeight = fmt->i_height;
    decoderDesc.OutputFormat = sys->render_fmt->formatTexture;

    UINT cfg_count;
    hr = sys->d3ddec->GetVideoDecoderConfigCount(&decoderDesc, &cfg_count );
    if (FAILED(hr)) {
        msg_Err(va, "GetVideoDecoderConfigCount failed. (hr=0x%lX)", hr);
        return VLC_EGENERIC;
    }

    /* List all configurations available for the decoder */
    D3D12_VIDEO_DECODER_HEAP_DESC cfg_list[cfg_count];
    for (unsigned i = 0; i < cfg_count; i++) {
        hr = sys->d3ddec->GetVideoDecoderConfig(&decoderDesc, i, &cfg_list[i] );
        if (FAILED(hr)) {
            msg_Err(va, "GetVideoDecoderConfig failed. (hr=0x%lX)", hr);
            return VLC_EGENERIC;
        }
    }

    msg_Dbg(va, "we got %d decoder configurations", cfg_count);

    /* Select the best decoder configuration */
    int cfg_score = 0;
    for (unsigned i = 0; i < cfg_count; i++) {
        const D3D12_VIDEO_DECODER_HEAP_DESC *cfg = &cfg_list[i];

        /* */
        msg_Dbg(va, "configuration[%d] ConfigBitstreamRaw %d",
                i, cfg->ConfigBitstreamRaw);

        /* */
        int score;
        if (cfg->ConfigBitstreamRaw == 1)
            score = 1;
        else if (codec_id == AV_CODEC_ID_H264 && cfg->ConfigBitstreamRaw == 2)
            score = 2;
        else
            continue;
        if (IsEqualGUID(&cfg->guidConfigBitstreamEncryption, &DXVA2_NoEncrypt))
            score += 16;

        if (cfg_score < score) {
            sys->cfg = *cfg;
            cfg_score = score;
        }
    }
    if (cfg_score <= 0) {
        msg_Err(va, "Failed to find a supported decoder configuration");
        return VLC_EGENERIC;
    }
#endif
    D3D12_VIDEO_DECODER_DESC decoderDesc = {};
    decoderDesc.NodeMask = sys->d3d_dev->gpuNodeMask;
    decoderDesc.Configuration.DecodeProfile = *sys->selected_decoder->guid;

    /* Create the decoder */
    ID3D12VideoDecoder *decoder;
    hr = sys->d3ddec->CreateVideoDecoder(&decoderDesc, IID_GRAPHICS_PPV_ARGS(&decoder));
    if (FAILED(hr)) {
        msg_Err(va, "CreateVideoDecoder failed. (hr=0x%lX)", hr);
        sys->hw.decoder = nullptr;
        return VLC_EGENERIC;
    }
    sys->hw.decoder = decoder;
    sys->hw.device = sys->d3d_dev->d3ddevice.Get();

    D3D12_VIDEO_DECODER_HEAP_DESC  cfg = { };
    cfg.NodeMask = sys->d3d_dev->gpuNodeMask;
    cfg.Configuration = decoderDesc.Configuration;
    cfg.DecodeWidth = fmt->i_width;
    cfg.DecodeHeight = fmt->i_height;
    cfg.Format = sys->render_fmt->formatTexture;
    cfg.MaxDecodePictureBufferCount = surface_count;
    if (fmt->i_frame_rate && fmt->i_frame_rate_base)
    {
        cfg.FrameRate = { fmt->i_frame_rate, fmt->i_frame_rate_base };
    }

    hr = sys->d3ddec->CreateVideoDecoderHeap(&cfg, IID_GRAPHICS_PPV_ARGS(&sys->hw.decoder_heap));
    if (FAILED(hr)) {
        msg_Err(va, "CreateVideoDecoderHeap failed. (hr=0x%lX)", hr);
        sys->hw.decoder_heap = nullptr;
        return VLC_EGENERIC;
    }
    msg_Dbg(va, "DxCreateDecoderSurfaces succeed");
    return VLC_SUCCESS;
}

static void DxDestroySurfaces(void *opaque)
{
    vlc_va_sys_t *sys = static_cast<vlc_va_sys_t *>(opaque);
    if (sys->p_texture)
        sys->p_texture->Release();
    if (sys->hw.decoder_heap)
        sys->hw.decoder_heap->Release();
    if (sys->hw.decoder)
        sys->hw.decoder->Release();
    if (sys->d3ddec)
        sys->d3ddec->Release();

    free(sys);
}
