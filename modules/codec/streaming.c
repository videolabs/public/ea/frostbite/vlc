/*****************************************************************************
 * aom.c: libaom encoder and decoder (AV1) module
 *****************************************************************************
 * Copyright (C) 2016 VLC authors and VideoLAN
 *
 * Authors: Tristan Matthews <tmatth@videolan.org>
 * Based on vpx.c by: Rafaël Carré <funman@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_codec.h>
#include <vlc_meta.h>
#include <vlc_plugin.h>
#include <vlc_modules.h>

static decoder_t *create_subdecoder(decoder_t *, const es_format_t *);
static void delete_subdecoder(decoder_t *);

/* Packetizer part */
static void delete_packetizer(decoder_t *p_pack)
{
    if(p_pack->p_module)
        module_unneed(p_pack, p_pack->p_module);
    es_format_Clean(&p_pack->fmt_in);
    es_format_Clean(&p_pack->fmt_out);
    if(p_pack->p_description)
        vlc_meta_Delete(p_pack->p_description);
    vlc_object_delete(p_pack);
}

static decoder_t *create_packetizer(vlc_object_t *parent,
                                    vlc_fourcc_t codec)
{
    decoder_t *p_pack = vlc_object_create(parent, sizeof(*p_pack));
    if(!p_pack)
        return NULL;
    p_pack->pf_decode = NULL;
    p_pack->pf_packetize = NULL;

    es_format_Init(&p_pack->fmt_in, VIDEO_ES, codec);
    es_format_Init(&p_pack->fmt_out, VIDEO_ES, 0);
    p_pack->fmt_in.b_packetized = false;

    p_pack->p_module = module_need( p_pack, "packetizer", NULL, false );
	if (!p_pack->p_module)
	{
		delete_packetizer(p_pack);
		return NULL;
	}
    return p_pack;
}

/* Decoders part */

typedef struct
{
    decoder_t *subdec;
    decoder_t *packetizer;
} decoder_sys_t;

struct decoder_owner
{
    decoder_t subdec;
    decoder_t *streamingdec;
};

static inline struct decoder_owner *dec_get_owner(decoder_t *dec)
{
    return container_of(dec, struct decoder_owner, subdec);
}

static void FlushDecoder(decoder_t *dec)
{
    decoder_sys_t *p_sys = dec->p_sys;
    if(p_sys->packetizer)
        p_sys->packetizer->pf_flush(p_sys->packetizer);
    if(p_sys->subdec)
        p_sys->subdec->pf_flush(p_sys->subdec);
}

static int PushToSubDecoder(decoder_t *dec, block_t *block, const es_format_t *fmtin)
{
    decoder_sys_t *sys = dec->p_sys;

    if(!sys->subdec || !es_format_IsSimilar(&sys->subdec->fmt_in, fmtin))
    {
        if(sys->subdec)
        {
            msg_Dbg(dec, "restarting subdecoder due to format change");
            sys->subdec->pf_decode(sys->subdec, NULL); /* Drain before restart */
            delete_subdecoder(sys->subdec);
        }
        sys->subdec = create_subdecoder(dec, fmtin);
        if(!sys->subdec)
            msg_Err(dec, "failed to create subdecoder for %4.4s", (const char *) &fmtin->i_codec);
        else
            msg_Dbg(dec, "new subdecoder created for %4.4s", (const char *) &fmtin->i_codec);
    }

    if(!sys->subdec)
    {
        msg_Warn(dec, "dropping block, no decoder available");
        block_Release(block);
        return VLC_EGENERIC;
    }

    return sys->subdec->pf_decode(sys->subdec, block);
}

static int PushToPacketizer(decoder_t *dec, block_t *block, const es_format_t *fmtin)
{
    decoder_sys_t *sys = dec->p_sys;
    block_t *in = block;
    if(sys->packetizer)
    {
        block_t *out;
        int ret = VLCDEC_SUCCESS;
        do
        {
            out = sys->packetizer->pf_packetize(sys->packetizer,
                                                block ? &in : NULL);
            if(out || (block == NULL))
                ret = PushToSubDecoder(dec, block ? out : NULL,
                                       &sys->packetizer->fmt_out);
        } while(out);
        return ret;
    }
    else return PushToSubDecoder(dec, block, fmtin);
}

static int Decode(decoder_t *dec, block_t *block)
{
    return PushToPacketizer(dec, block, &dec->fmt_in);
}

static vlc_decoder_device * get_no_device( decoder_t *subdec )
{
    struct decoder_owner *owner = dec_get_owner(subdec);
    // no decoder device for this test
    return owner->streamingdec->cbs->video.get_device(owner->streamingdec);
}

static void queue_video(decoder_t *subdec, picture_t *pic)
{
    struct decoder_owner *owner = dec_get_owner(subdec);
    owner->streamingdec->cbs->video.queue(owner->streamingdec, pic);
}

static void queue_cc(decoder_t *subdec, block_t *p_block, const decoder_cc_desc_t *desc)
{
    struct decoder_owner *owner = dec_get_owner(subdec);
    owner->streamingdec->cbs->video.queue_cc(owner->streamingdec, p_block, desc);
}

static int format_update( decoder_t *subdec, vlc_video_context *ctx )
{
    struct decoder_owner *owner = dec_get_owner(subdec);
    es_format_Clean(&owner->streamingdec->fmt_out);
    es_format_Init(&owner->streamingdec->fmt_out, subdec->fmt_out.i_cat, 0);
    es_format_Copy(&owner->streamingdec->fmt_out, &subdec->fmt_out);
    return owner->streamingdec->cbs->video.format_update(owner->streamingdec, ctx);
}

static vlc_tick_t  get_display_date( decoder_t *subdec, vlc_tick_t a, vlc_tick_t b )
{
    struct decoder_owner *owner = dec_get_owner(subdec);
    return owner->streamingdec->cbs->video.get_display_date(owner->streamingdec, a, b);
}

static const struct decoder_owner_callbacks subdec_video_cbs =
{
    .video = {
        .get_device = get_no_device,
        .queue = queue_video,
        .queue_cc = queue_cc,
        .format_update = format_update,
        .get_display_date = get_display_date,
    },
};

static void delete_subdecoder(decoder_t *subdec)
{
    if ( subdec->p_module != NULL )
    {
        module_unneed(subdec, subdec->p_module);
        subdec->p_module = NULL;
    }
    es_format_Clean(&subdec->fmt_in);
    es_format_Clean(&subdec->fmt_out);
    vlc_object_delete(subdec);
}

static decoder_t *create_subdecoder(decoder_t *parent, const es_format_t *fmt)
{
    struct decoder_owner *owner = vlc_object_create(parent, sizeof(*owner));
    if(!owner)
        return NULL;
    owner->streamingdec = parent;
    owner->subdec.cbs = &subdec_video_cbs;
    owner->subdec.pf_decode = NULL;
    owner->subdec.pf_get_cc = NULL;
    owner->subdec.pf_packetize = NULL;
    owner->subdec.pf_flush = NULL;
    es_format_Copy( &owner->subdec.fmt_in, fmt );
    es_format_Init( &owner->subdec.fmt_out, fmt->i_cat, 0 );
    owner->subdec.p_module = module_need(&owner->subdec, "video decoder", NULL, false);
    if(!owner->subdec.p_module)
    {
        es_format_Clean(&owner->subdec.fmt_in);
        es_format_Clean(&owner->subdec.fmt_out);
        vlc_object_delete(&owner->subdec);
        return NULL;
    }
    return &owner->subdec;
}

void Streaming_CloseDecoder(vlc_object_t *p_this)
{
    decoder_t *dec = (decoder_t *)p_this;
    decoder_sys_t *sys = dec->p_sys;

    if(sys->subdec)
        delete_subdecoder(sys->subdec);

    if(sys->packetizer)
        delete_packetizer(sys->packetizer);

    free(sys);
}

int Streaming_OpenDecoder(vlc_object_t* p_this)
{
    decoder_t *dec = (decoder_t *)p_this;

    if (dec->fmt_in.i_cat != VIDEO_ES)
        return VLC_EGENERIC;

    for (vlc_object_t *obj = p_this; obj != NULL; obj = vlc_object_parent(obj))
    {
        vlc_value_t val;
        if(var_GetChecked( obj, "streaming-is-subdec", VLC_VAR_BOOL, &val ) == VLC_SUCCESS)
            return VLC_EGENERIC;
    }

    decoder_sys_t *sys = malloc(sizeof(*sys));
    if (!sys)
        return VLC_ENOMEM;
    dec->p_sys = sys;

    dec->pf_decode = Decode;
    dec->pf_flush = FlushDecoder;

    var_Create(p_this, "streaming-is-subdec", VLC_VAR_BOOL);
    var_SetBool(p_this, "streaming-is-subdec", true);

    sys->packetizer = create_packetizer(VLC_OBJECT(dec), dec->fmt_in.i_codec);

    if(!sys->packetizer)
    {
        sys->subdec = create_subdecoder(dec, &dec->fmt_in);
        if(!sys->subdec)
        {
            Streaming_CloseDecoder(p_this);
            return VLC_EGENERIC;
        }
    }
    else sys->subdec = NULL; /* will be created by packetizer */

    return VLC_SUCCESS;
}

