/*****************************************************************************
 * expeller.c : text rendering not really doing text rendering
 *****************************************************************************
 * Copyright (C) 2021 VideoLabs, VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_filter.h>
#include <vlc_picture.h>
#include <vlc_subpicture.h>
#include <vlc_memstream.h>

static int RenderText( filter_t *p_filter,
                       subpicture_region_t *p_region_out,
                       subpicture_region_t *p_region_in,
                       const vlc_fourcc_t *p_chroma_list )
{
    VLC_UNUSED(p_filter);
    VLC_UNUSED(p_chroma_list);

    if(p_region_in->fmt.i_chroma != VLC_CODEC_TEXT)
        return VLC_EGENERIC;

    struct vlc_memstream ms;
    if(vlc_memstream_open(&ms))
        return VLC_EGENERIC;

    for(const text_segment_t *p = p_region_in->p_text; p; p = p->p_next)
        if(p->psz_text)
            vlc_memstream_puts(&ms, p->psz_text);
    vlc_memstream_putc(&ms, '\0');

    if(vlc_memstream_close(&ms))
        return VLC_EGENERIC;

    video_format_t fmt;
    video_format_Init( &fmt, VLC_CODEC_GREY );
    fmt.i_bits_per_pixel = 8;
    fmt.i_width = 512;
    fmt.i_height = 1 + (ms.length + sizeof(uint32_t)) / 512;

    picture_t *p_picture = picture_NewFromFormat( &fmt );
    if( !p_picture )
    {
        video_format_Clean( &fmt );
        free(ms.ptr);
        return VLC_EGENERIC;
    }
    SetDWBE( p_picture->p[0].p_pixels, ms.length );
    memcpy( p_picture->p[0].p_pixels + sizeof(uint32_t), ms.ptr, ms.length );
    free( ms.ptr );

    fmt.i_chroma = p_picture->format.i_chroma = VLC_CODEC_META_INTERNAL;
    video_format_Copy( &p_region_out->fmt, &fmt );
    video_format_Clean( &fmt );

    p_region_out->p_picture = p_picture;

    return VLC_SUCCESS;
}

static const struct vlc_filter_operations filter_ops = {
    .render = RenderText,
};

int textExpeller_OpenRenderer( filter_t *p_filter )
{
    p_filter->ops = &filter_ops;
    return VLC_SUCCESS;
}

