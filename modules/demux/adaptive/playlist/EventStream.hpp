/*****************************************************************************
 * EventStream.hpp
 *****************************************************************************
 * Copyright (C) 2021 VideoLabs, VideoLAN and VLC Authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef EVENTSTREAM_HPP_
#define EVENTSTREAM_HPP_

#include <string>
#include <list>
#include <vector>
#include <memory>
#include <set>
#include <vlc_common.h>

namespace adaptive
{
    namespace playlist
    {
        class EventStreamEvent
        {
            public:
                EventStreamEvent() = delete;
                EventStreamEvent(const std::string &, const std::string &,
                              vlc_tick_t = VLC_TICK_INVALID);
                const std::string & getNamespace() const;
                const std::string & getID() const;
                vlc_tick_t getTime() const;
                void setDuration(vlc_tick_t);
                vlc_tick_t getDuration() const;
                void setPayload(const std::vector<uint8_t> &);
                const std::vector<uint8_t> & getPayload() const;

            protected:
                std::vector<uint8_t> payload;
                vlc_tick_t mediatime;
                vlc_tick_t duration;
                std::string id;
                std::string ns;
        };

        using EventStreamEventPtr = std::shared_ptr<EventStreamEvent>;
        using EventStreamEvents = std::list<EventStreamEventPtr>;
    }
}

#endif
