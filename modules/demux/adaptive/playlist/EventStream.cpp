/*****************************************************************************
 * EventStream.cpp
 *****************************************************************************
 * Copyright (C) 2021 VideoLabs, VideoLAN and VLC Authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "EventStream.hpp"

using namespace adaptive;
using namespace adaptive::playlist;

EventStreamEvent::EventStreamEvent(const std::string &ns_, const std::string &id_, vlc_tick_t t)
{
    ns = ns_;
    id = id_;
    mediatime = t;
    duration = -1;
}

const std::string & EventStreamEvent::getID() const
{
    return id;
}

const std::string & EventStreamEvent::getNamespace() const
{
    return ns;
}

vlc_tick_t EventStreamEvent::getTime() const
{
    return mediatime;
}

void EventStreamEvent::setDuration(vlc_tick_t d)
{
    duration = d;
}

vlc_tick_t EventStreamEvent::getDuration() const
{
    return duration;
}

void EventStreamEvent::setPayload(const std::vector<uint8_t> &p)
{
    payload = p;
}

const std::vector<uint8_t> & EventStreamEvent::getPayload() const
{
    return payload;
}
