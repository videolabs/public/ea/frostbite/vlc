/*****************************************************************************
 * EventStreamRegistry.cpp
 *****************************************************************************
 * Copyright (C) 2021 VideoLabs, VideoLAN and VLC Authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "EventStreamRegistry.hpp"
#include "Time.hpp"

#include <algorithm>
#include <vector>

using namespace adaptive;
using namespace adaptive::playlist;

EventStreamRegistry::EventStreamRegistry()
{

}

EventStreamRegistry::~EventStreamRegistry()
{

}

void EventStreamRegistry::registerNamespace(const std::string &ns)
{
    registered_namespaces.insert(ns);
}

void EventStreamRegistry::appendEvent(EventStreamEventPtr e)
{
    if(!accepts(e->getNamespace()))
        return;

    events.push_front(Entry(e,false));
}

bool EventStreamRegistry::accepts(const std::string &ns) const
{
    return registered_namespaces.empty() ||
           registered_namespaces.find(ns) != registered_namespaces.end();
}

void EventStreamRegistry::triggerEvents(const Times &start, const Times &,
                                           std::function<void(const EventStreamEvent &)> func)
{
    std::list<Entry>::iterator it = events.begin();
    while(it != events.end())
    {
        const vlc_tick_t eventMediaTime = (*it).first->getTime();
        bool trigger = (eventMediaTime == VLC_TICK_INVALID) ||
                        start.segment.media >= eventMediaTime;
        if(!(*it).second && trigger)
        {
            (*it).second = true;
            func(*(*it).first);
        }
        if((*it).second &&
           ((eventMediaTime == VLC_TICK_INVALID) ||
            (start.segment.media == VLC_TICK_INVALID ||
             start.segment.media < eventMediaTime)))
        {
            events.erase(it++);
        }
        else ++it;
    }
}

void EventStreamRegistry::reset()
{
    events.clear();
}
