/*
 * HTTPConnection.hpp
 *****************************************************************************
 * Copyright (C) 2010 - 2011 Klagenfurt University
 *               2014 - 2015 VideoLAN and VLC Authors
 *
 * Created on: Aug 10, 2010
 * Authors: Christopher Mueller <christopher.mueller@itec.uni-klu.ac.at>
 *          Christian Timmerer  <christian.timmerer@itec.uni-klu.ac.at>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef HTTPCONNECTION_H_
#define HTTPCONNECTION_H_

#include "ConnectionParams.hpp"
#include "BytesRange.hpp"
#include <modules/demux/adaptive/http/IThirdPartyConnection.hpp>
#include <vlc_common.h>
#include <string>

namespace adaptive
{
    namespace http
    {
        class Transport;
        class AuthStorage;

        class AbstractConnection
        {
            public:
                AbstractConnection(vlc_object_t *);
                virtual ~AbstractConnection() = default;

                virtual bool    prepare     (const ConnectionParams &);
                virtual bool    canReuse     (const ConnectionParams &) const = 0;

                virtual RequestStatus request(const std::string& path,
                                              const BytesRange & = BytesRange()) = 0;
                virtual ssize_t read        (void *p_buffer, size_t len) = 0;

                virtual size_t  getContentLength() const;
                virtual const std::string & getContentType() const;
                virtual void    setUsed( bool ) = 0;

                virtual ConnectionParams getRedirection() const = 0;
                virtual bool getRedirectionSupported() const = 0;

            protected:
                vlc_object_t      *p_object;
                ConnectionParams   params;
                bool               available;
                size_t             contentLength;
                std::string        contentType;
                BytesRange         bytesRange;
                size_t             bytesRead;
        };

#ifdef VLC_FB_FORCE_DISABLE_NETWORK

        class HTTPConnection : public AbstractConnection
        {
            public:
                HTTPConnection(vlc_object_t *, AuthStorage *,  Transport *,
                               const ConnectionParams &);
                virtual ~HTTPConnection();

                virtual bool    canReuse     (const ConnectionParams &) const override;
                virtual RequestStatus request(const std::string& path,
                                              const BytesRange & = BytesRange()) override;
                virtual ssize_t read        (void *p_buffer, size_t len) override;

                void setUsed( bool ) override;
                virtual ConnectionParams getRedirection() const override;
                virtual bool getRedirectionSupported() const override;
                static const unsigned MAX_REDIRECTS = 3;

            protected:
                bool    connected   () const;
                bool    connect     ();
                void    disconnect  ();
                bool    send        (const void *buf, size_t size);
                bool    send        (const std::string &data);

                void    onHeader    (const std::string &line,
                                             const std::string &value);
                std::string extraRequestHeaders() const;
                std::string buildRequestHeader(const std::string &path) const;

                ssize_t         readChunk   (void *p_buffer, size_t len);
                RequestStatus parseReply();
                std::string readLine();
                std::string useragent;
                std::string referer;

                AuthStorage        *authStorage;
                ConnectionParams    locationparams;
                ConnectionParams    proxyparams;
                bool                connectionClose;
                bool                chunked;
                bool                chunked_eof;
                size_t              chunkLength;
                bool                queryOk;
                int                 retries;
                static const int    retryCount = 5;

            private:
                Transport *transport;
       };
#endif

        class ThirdPartyConnection : public AbstractConnection, public IThirdPartyConnectionCb
        {
            public:
                ThirdPartyConnection(vlc_object_t *, AuthStorage *, IThirdPartyConnection* );
                virtual ~ThirdPartyConnection();

                virtual bool    canReuse     (const ConnectionParams &) const override;
                virtual RequestStatus request(const std::string& path,
                                              const BytesRange & = BytesRange()) override;
                virtual ssize_t read        (void *p_buffer, size_t len) override;

                void setUsed( bool ) override;
                virtual ConnectionParams getRedirection() const override;
                virtual bool getRedirectionSupported() const override;

                virtual void onCookieReceived(const std::string& cookie,
                                              const std::string& url ) override;

            protected:
                AuthStorage        *authStorage;
                ConnectionParams    locationparams;
                ConnectionParams    proxyparams;
                bool                connectionClose;
                bool                chunked;
                bool                chunked_eof;
                size_t              chunkLength;
                bool                queryOk;
                int                 retries;
                static const int    retryCount = 5;

            private:
                IThirdPartyConnection *m_conn;
                AuthStorage* m_authStorage;
       };

#ifdef VLC_FB_FORCE_DISABLE_NETWORK

       class StreamUrlConnection : public AbstractConnection
       {
            public:
                StreamUrlConnection(vlc_object_t *);
                virtual ~StreamUrlConnection();

                virtual bool    canReuse     (const ConnectionParams &) const override;

                virtual RequestStatus request(const std::string& path,
                                              const BytesRange & = BytesRange()) override;
                virtual ssize_t read        (void *p_buffer, size_t len) override;

                virtual void    setUsed( bool ) override;

                virtual ConnectionParams getRedirection() const override;
                virtual bool getRedirectionSupported() const override;

            protected:
                void reset();
                stream_t *p_streamurl;
       };

#endif

       class AbstractConnectionFactory
       {
           public:
               virtual ~AbstractConnectionFactory() = default;
               virtual AbstractConnection * createConnection(vlc_object_t *, const ConnectionParams &) = 0;
       };

#ifdef VLC_FB_FORCE_DISABLE_NETWORK

       class NativeConnectionFactory : public AbstractConnectionFactory
       {
           public:
               NativeConnectionFactory( AuthStorage * );
               virtual AbstractConnection * createConnection(vlc_object_t *, const ConnectionParams &) override;
           private:
               AuthStorage *authStorage;
       };

       class StreamUrlConnectionFactory : public AbstractConnectionFactory
       {
           public:
               virtual AbstractConnection * createConnection(vlc_object_t *, const ConnectionParams &) override;
       };

#endif

       class ThirdPartyConnectionFactory : public AbstractConnectionFactory
       {
           public:
               ThirdPartyConnectionFactory( AuthStorage * );
               virtual AbstractConnection* createConnection( vlc_object_t*, const ConnectionParams& ) override;

           private:
               AuthStorage* m_authStorage;
       };
    }
}

#endif /* HTTPCONNECTION_H_ */
