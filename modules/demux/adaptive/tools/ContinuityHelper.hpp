/*
 * ContinuityHelper.hpp
 *****************************************************************************
 * Copyright © 2021 - VideoLabs, VideoLAN and VLC Authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef CONTINUITYHELPER_HPP
#define CONTINUITYHELPER_HPP

#include "MovingAverage.hpp"
#include "../Time.hpp"
#include <vlc_common.h>
#include <vector>

namespace adaptive
{
    class ContinuityHelper
    {
        public:
            enum class SampleType
            {
                Video = 0,
                Audio = 1
            };

            class ContinuityReference
            {
                friend class ContinuityHelper;

                public:
                    ContinuityReference();
                    Times getContinuityReferenceTimes(SampleType) const;

                private:
                    std::vector<Times> endtimes;
            };

            ContinuityHelper();
            ~ContinuityHelper() = default;
            void addSample(SampleType, const Times &, vlc_tick_t);
            ContinuityReference makeContinuityReferenceTime() const;

        private:
            struct counters
            {
                Times times;
                vlc_tick_t duration;
                MovingAverage<vlc_tick_t> avg;
            };
            std::vector<struct counters> samples;
    };
}

#endif
