/*
 * ContinuityHelper.cpp
 *****************************************************************************
 * Copyright © 2021 - VideoLabs, VideoLAN and VLC Authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "ContinuityHelper.hpp"

using namespace adaptive;
using ContinuityReference = ContinuityHelper::ContinuityReference;

ContinuityReference::ContinuityReference()
{
    endtimes.resize((int)SampleType::Audio + 1);
    for(auto &end : endtimes)
        end = Times();
}

Times ContinuityReference::getContinuityReferenceTimes(SampleType type) const
{
    Times ret;
    if(endtimes[(int)type].continuous == VLC_TICK_INVALID)
        return ret;
    vlc_tick_t offset = 0;
    if(endtimes[(int)SampleType::Video].continuous != VLC_TICK_INVALID)
        offset = endtimes[(int)type].continuous - endtimes[(int)SampleType::Video].continuous;
    ret = endtimes[(int)type];
    ret.offsetBy(offset);
    return ret;
}

ContinuityHelper::ContinuityHelper()
{
    samples.resize((int)SampleType::Audio + 1);
    for(auto &sample : samples)
    {
        sample.times = Times();
        sample.duration = 0;
    }
}

void ContinuityHelper::addSample(SampleType type, const Times &t, vlc_tick_t duration)
{
    if(duration == 0 && t.continuous != VLC_TICK_INVALID &&
       samples[(int)type].times.continuous != VLC_TICK_INVALID)
         duration = t.continuous - samples[(int)type].times.continuous;
    if(duration > 0)
        samples[(int)type].duration = samples[(int)type].avg.push(duration);
    if(t.continuous != VLC_TICK_INVALID)
        samples[(int)type].times = t;
}

ContinuityReference ContinuityHelper::makeContinuityReferenceTime() const
{
    ContinuityReference ref;
    for(decltype(samples)::size_type i = 0; i<samples.size(); i++)
    {
        if(samples[i].times.continuous != VLC_TICK_INVALID)
        {
            ref.endtimes[i] = samples[i].times;
            ref.endtimes[i].offsetBy(samples[i].duration);
        }
    }
    return ref;
}
