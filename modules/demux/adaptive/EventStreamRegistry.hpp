/*****************************************************************************
 * EventStreamRegistry.hpp
 *****************************************************************************
 * Copyright (C) 2021 VideoLabs, VideoLAN and VLC Authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef EVENTSTREAMREGISTRY_HPP_
#define EVENTSTREAMREGISTRY_HPP_

#include "playlist/EventStream.hpp"

#include <functional>
#include <set>
#include <list>

namespace adaptive
{
    using namespace playlist;

    class Times;

    class EventStreamRegistry
    {
        public:
            using Entry = std::pair<EventStreamEventPtr, bool>;

            EventStreamRegistry();
            virtual ~EventStreamRegistry();
            void registerNamespace(const std::string &);
            virtual void appendEvent(EventStreamEventPtr);
            bool accepts(const std::string &) const;
            void triggerEvents(const Times &, const Times &,
                               std::function<void(const EventStreamEvent &)>);
            void reset();

        protected:
            std::set<std::string> registered_namespaces;
            std::list<Entry> events;
    };

}

#endif
