/*****************************************************************************
 * MetadataRegistry.cpp
 *****************************************************************************
 * Copyright (C) 2021 VideoLabs, VideoLAN and VLC Authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "MetadataRegistry.hpp"
#include "playlist/MetadataEvent.hpp"

#include <algorithm>

using namespace hls;
using namespace hls::playlist;
using namespace adaptive::playlist;

MetadataRegistry::MetadataRegistry()
    : EventStreamRegistry()
{

}

MetadataRegistry::~MetadataRegistry()
{

}

void MetadataRegistry::appendEvent(EventStreamEventPtr e)
{
    if(!accepts(e->getNamespace()))
        return;

    auto it = std::find_if(events.begin(), events.end(),
                           [e] (const Entry &entry)
    {
        MetadataEventPtr he = std::static_pointer_cast<MetadataEvent>(e);
        MetadataEventPtr hentry = std::static_pointer_cast<MetadataEvent>(entry.first);
        return hentry->equals(*he);
    });
    if(it != events.end())
    {
        MetadataEventPtr he = std::static_pointer_cast<MetadataEvent>(e);
        MetadataEventPtr hentry = std::static_pointer_cast<MetadataEvent>((*it).first);
        if(!hentry->isUpdatedBy(*he))
            return;
        /* remove same, outdated event */
        events.erase(it);
    }

    /* Apply the end-on-next flag rule */
    for(it = events.begin(); it != events.end();)
    {
        auto cur = std::static_pointer_cast<MetadataEvent>((*it).first);
        if(cur->getEndsOnNext() && cur->getNamespace() == e->getNamespace())
            events.erase(it++);
        else
            ++it;
    }

    events.push_front(Entry(e,false));
}
