/*****************************************************************************
 * MetadataEvent.cpp
 *****************************************************************************
 * Copyright (C) 2021 VideoLabs, VideoLAN and VLC Authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "MetadataEvent.hpp"

using namespace hls;
using namespace hls::playlist;

MetadataEvent::MetadataEvent(const std::string &ns_, const std::string &id_, vlc_tick_t t)
    : EventStreamEvent(ns_, id_, t)
{
    end_on_next = false;
}

void MetadataEvent::setEndsOnNext()
{
    end_on_next = true;
}

bool MetadataEvent::getEndsOnNext() const
{
    return end_on_next;
}

void MetadataEvent::addMeta(const std::string &k, const std::string &v)
{
    metas.insert(std::pair<std::string, std::string>(k, v));
}

bool MetadataEvent::equals(const MetadataEvent &other) const
{
    return (id == other.id && ns == other.ns);
}

bool MetadataEvent::isUpdatedBy(const MetadataEvent &other) const
{
    for(auto m : other.metas)
        if(metas.find(m.first) == metas.end())
            return true;
    return false;
}

const Metadata & MetadataEvent::allMetadatas() const
{
    return metas;
}

const std::string MetadataEvent::getMeta(const std::string &k) const
{
    auto it = metas.find(k);
    return (it != metas.end()) ? it->second : std::string();
}
