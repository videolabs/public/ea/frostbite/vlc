/*****************************************************************************
 * MetadataEvent.hpp
 *****************************************************************************
 * Copyright (C) 2021 VideoLabs, VideoLAN and VLC Authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifndef METADATAEVENT_HPP_
#define METADATAEVENT_HPP_

#include "../../adaptive/playlist/EventStream.hpp"

#include <string>
#include <map>

namespace hls
{
    namespace playlist
    {
        using namespace adaptive::playlist;

        using Metadata = std::map<std::string, std::string>;

        class MetadataEvent : public EventStreamEvent
        {
            public:
                MetadataEvent() = delete;
                MetadataEvent(const std::string &, const std::string &,
                              vlc_tick_t = VLC_TICK_INVALID);
                void setEndsOnNext();
                bool getEndsOnNext() const;
                const Metadata & allMetadatas() const;
                const std::string getMeta(const std::string &) const;
                void addMeta(const std::string &, const std::string &);
                bool equals(const MetadataEvent &) const;
                bool isUpdatedBy(const MetadataEvent &) const;

            private:
                bool end_on_next;
                Metadata metas;
        };

        using MetadataEventPtr = std::shared_ptr<MetadataEvent>;
        using MetadataEvents = std::list<MetadataEventPtr>;
    }
}

#endif
