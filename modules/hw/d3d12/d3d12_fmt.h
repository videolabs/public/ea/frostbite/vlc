/*****************************************************************************
 * d3d12_fmt.h : D3D12 helper calls and structures
 *****************************************************************************
 * Copyright © 2021 VLC authors, VideoLAN and VideoLabs
 *
 * Authors: Steve Lhomme <robux4@videolabs.io>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef VLC_HW_D3D12_FMT_H_
#define VLC_HW_D3D12_FMT_H_

#include <vlc_codec.h>

#include "../../video_chroma/dxgi_fmt.h"

#if !(defined(_GAMING_XBOX) || defined(_XBOX_ONE))
#include <d3d12.h>
#endif // !_GAMING_XBOX && !_XBOX_ONE

#ifndef IID_GRAPHICS_PPV_ARGS
#define IID_GRAPHICS_PPV_ARGS(ppType) IID_PPV_ARGS(ppType)
#endif

#include <wrl/client.h>

static inline bool is_d3d12_opaque(vlc_fourcc_t chroma)
{
    return chroma == VLC_CODEC_D3D12_OPAQUE ||
           chroma == VLC_CODEC_D3D12_OPAQUE_10B ||
           chroma == VLC_CODEC_D3D12_OPAQUE_RGBA ||
           chroma == VLC_CODEC_D3D12_OPAQUE_BGRA;
}

typedef struct
{
    Microsoft::WRL::ComPtr<ID3D12Device1> d3ddevice;
    Microsoft::WRL::ComPtr<IDXGIAdapter>  adapter;
    UINT                                  gpuNodeIndex;
    UINT                                  gpuNodeMask;
} d3d12_device_t;

typedef struct
{
    d3d12_device_t           d3d_dev;
} d3d12_decoder_device_t;

static inline d3d12_decoder_device_t *GetD3D12OpaqueDevice(vlc_decoder_device *device)
{
    if (device == NULL || device->type != VLC_DECODER_DEVICE_D3D12)
        return NULL;
    return (d3d12_decoder_device_t *)device->opaque;
}

static inline d3d12_decoder_device_t *GetD3D12OpaqueContext(vlc_video_context *vctx)
{
    vlc_decoder_device *device = vctx ? vlc_video_context_HoldDevice(vctx) : NULL;
    if (unlikely(device == NULL))
        return NULL;
    d3d12_decoder_device_t *res = NULL;
    if (device->type == VLC_DECODER_DEVICE_D3D12)
    {
        assert(device->opaque != NULL);
        res = GetD3D12OpaqueDevice(device);
    }
    vlc_decoder_device_Release(device);
    return res;
}


typedef struct
{
    DXGI_FORMAT         format;
} d3d12_video_context_t;

vlc_video_context *D3D12CreateVideoContext(vlc_decoder_device *, DXGI_FORMAT);

static inline d3d12_video_context_t *GetD3D12ContextPrivate(vlc_video_context *vctx)
{
    return (d3d12_video_context_t *) vlc_video_context_GetPrivate( vctx, VLC_VIDEO_CONTEXT_D3D12VA );
}


d3d12_decoder_device_t *D3D12_CreateDevice(vlc_object_t*);
void D3D12_ReleaseDevice(d3d12_decoder_device_t *);

void D3D12_LogResources(const d3d12_device_t &);

bool D3D12_DeviceSupportsFormat(d3d12_device_t *, DXGI_FORMAT, UINT supportFlags);

const d3d_format_t *FindD3D12Format(d3d12_device_t *d3d_dev,
                                    vlc_fourcc_t i_src_chroma,
                                    int rgb_yuv,
                                    uint8_t bits_per_channel,
                                    uint8_t widthDenominator,
                                    uint8_t heightDenominator,
                                    int cpu_gpu,
                                    UINT supportFlags);

#ifndef NDEBUG
void D3D12_ObjectSetName(ID3D12Object *, const wchar_t *fmt, ...);
#define D3D12_ObjectSetName(o,s, ...)  D3D12_ObjectSetName(o, L"VLC " s, ##__VA_ARGS__)
#else
#define D3D12_ObjectSetName(o,s, ...)
#endif


typedef struct
{
    ID3D12CommandAllocator    *m_commandAllocator;
    ID3D12GraphicsCommandList *m_commandList;
} d3d12_commands_t;

HRESULT D3D12_CommandListCreateGraphic(vlc_object_t *, d3d12_device_t *, d3d12_commands_t *);
void D3D12_CommandListRelease(d3d12_commands_t *);


/* for VLC_CODEC_D3D12_OPAQUE* */
typedef struct
{
    ID3D12Resource            *resource[DXGI_MAX_SHADER_VIEW];
    unsigned                  resource_plane[DXGI_MAX_SHADER_VIEW];
    unsigned                  slice_index;
    void                      (*pf_wait_display_ready)(picture_context_t &, ID3D12CommandQueue *);
} picture_sys_d3d12_t;

struct d3d12_pic_context
{
    picture_context_t         s;
    picture_sys_d3d12_t       picsys;
};

void d3d12_pic_context_destroy(picture_context_t *);

#define D3D12_PICCONTEXT_FROM_PICCTX(pic_ctx)  \
    container_of(pic_ctx, struct d3d12_pic_context, s)


void AcquireD3D12PictureSys(picture_sys_d3d12_t *);
void ReleaseD3D12PictureSys(picture_sys_d3d12_t *);
picture_sys_d3d12_t *ActiveD3D12PictureSys(picture_t *);

#if (defined(_GAMING_XBOX) || defined(_XBOX_ONE))
#define isXboxHardware(x)  (true)
#else
#define isXboxHardware(x)  (false)
#endif

#endif /* include-guard */
