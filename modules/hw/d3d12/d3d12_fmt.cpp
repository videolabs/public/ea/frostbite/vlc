/*****************************************************************************
 * d3d12_fmt.c : D3D12 format abstractions
 *****************************************************************************
 * Copyright © 2021 VLC authors, VideoLAN and VideoLabs
 *
 * Authors: Steve Lhomme <robux4@videolabs.io>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_codec.h>
#include <vlc_plugin.h>

#include <vlc/libvlc.h>
#include <vlc/libvlc_picture.h>
#include <vlc/libvlc_media.h>
#include <vlc/libvlc_renderer_discoverer.h>
#include <vlc/libvlc_media_player.h>

#include "d3d12_fmt.h"
#if !(defined(_GAMING_XBOX) || defined(_XBOX_ONE))
#include <dxgi1_3.h>
#if !defined(NDEBUG)
# include <d3d12sdklayers.h>
#endif
#endif // !_GAMING_XBOX && !_XBOX_ONE

typedef struct {
    struct {
        void                            *opaque;
        libvlc_video_output_cleanup_cb  cleanupDeviceCb;
    } external;

    d3d12_decoder_device_t              dec_device;
} d3d12_decoder_device;

d3d12_decoder_device_t *(D3D12_CreateDevice)(vlc_object_t *obj)
{
    d3d12_decoder_device *sys = new d3d12_decoder_device();
    if (unlikely(sys==nullptr))
        return nullptr;

    HRESULT hr;

#if !(defined(_GAMING_XBOX) || defined(_XBOX_ONE)) && !VLC_WINSTORE_APP
    UINT dxgiFactoryFlags = 0;
#if !defined(NDEBUG)
    if (IsDebuggerPresent())
    {
        Microsoft::WRL::ComPtr<ID3D12Debug> debug;
        hr = D3D12GetDebugInterface(IID_GRAPHICS_PPV_ARGS(debug.GetAddressOf()));
        if (SUCCEEDED(hr))
        {
            debug->EnableDebugLayer();
            dxgiFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;

            if (var_InheritInteger(obj, "verbose") >= 5)
            {
                // this makes the code slower
                Microsoft::WRL::ComPtr<ID3D12Debug1> debug1;
                hr = debug.CopyTo(IID_GRAPHICS_PPV_ARGS(debug1.GetAddressOf()));
                if (SUCCEEDED(hr))
                    debug1->SetEnableGPUBasedValidation(true);
            }
        }
    }
#endif // !NDEBUG
#endif

    Microsoft::WRL::ComPtr<IDXGIFactory1> factory;
    libvlc_video_engine_t engineType = (libvlc_video_engine_t)var_InheritInteger( obj, "vout-cb-type" );
    libvlc_video_output_setup_cb setupDeviceCb = nullptr;
    if (engineType == libvlc_video_engine_d3d12 || engineType == libvlc_video_engine_d3d12_raw)
        setupDeviceCb = (libvlc_video_output_setup_cb)var_InheritAddress( obj, "vout-cb-setup" );
    if ( setupDeviceCb != nullptr)
    {
        /* decoder device coming from the external app */
        sys->external.opaque          = var_InheritAddress( obj, "vout-cb-opaque" );
        sys->external.cleanupDeviceCb = (libvlc_video_output_cleanup_cb)var_InheritAddress( obj, "vout-cb-cleanup" );
        libvlc_video_setup_device_cfg_t cfg = { };
        cfg.hardware_decoding = true;

        libvlc_video_setup_device_info_t out = { };
        if (!setupDeviceCb( &sys->external.opaque, &cfg, &out ))
        {
            if (sys->external.cleanupDeviceCb)
                sys->external.cleanupDeviceCb( sys->external.opaque );
            goto error;
        }
        if (unlikely(out.d3d12.device == nullptr))
            goto error;
        sys->dec_device.d3d_dev.d3ddevice = static_cast<ID3D12Device1 *>(out.d3d12.device);
        sys->dec_device.d3d_dev.gpuNodeMask = out.d3d12.gpu_node_mask;
        sys->dec_device.d3d_dev.gpuNodeIndex = out.d3d12.gpu_node_index;
        if (out.d3d12.adapter)
        {
            sys->dec_device.d3d_dev.adapter = static_cast<IDXGIAdapter *>(out.d3d12.adapter);
        }
        else
        {
#if !(defined(_GAMING_XBOX) || defined(_XBOX_ONE)) && !defined(VLC_WINSTORE_APP)
            hr = CreateDXGIFactory2(dxgiFactoryFlags, IID_GRAPHICS_PPV_ARGS(factory.GetAddressOf()));
            if (FAILED(hr))
                goto error;

            hr = factory->EnumAdapters(0, sys->dec_device.d3d_dev.adapter.GetAddressOf());
            if (FAILED(hr))
                goto error;
#else // TODO_D3D12 _GAMING_XBOX || _XBOX_ONE
            hr = S_FALSE;
            goto error;
#endif
        }
    }
    else
    {
#if !(defined(_GAMING_XBOX) || defined(_XBOX_ONE)) && !VLC_WINSTORE_APP
        hr = CreateDXGIFactory2(dxgiFactoryFlags, IID_GRAPHICS_PPV_ARGS(factory.GetAddressOf()));
        if (FAILED(hr))
            goto error;

        hr = factory->EnumAdapters(0, sys->dec_device.d3d_dev.adapter.GetAddressOf());
        if (FAILED(hr))
            goto error;

        assert(sys->dec_device.d3d_dev.adapter.Get());
        hr = D3D12CreateDevice(sys->dec_device.d3d_dev.adapter.Get(), D3D_FEATURE_LEVEL_12_0,
                               IID_GRAPHICS_PPV_ARGS(sys->dec_device.d3d_dev.d3ddevice.GetAddressOf()));
        if (FAILED(hr))
            goto error;

        // use the last node to avoid colliding with other apps
        sys->dec_device.d3d_dev.gpuNodeIndex = sys->dec_device.d3d_dev.d3ddevice->GetNodeCount() - 1;
        sys->dec_device.d3d_dev.gpuNodeMask = (1 << sys->dec_device.d3d_dev.gpuNodeIndex);
#else  // TODO_D3D12 _GAMING_XBOX || _XBOX_ONE
        hr = S_FALSE;
        goto error;
#endif
    }

    return &sys->dec_device;
error:
    sys->dec_device.d3d_dev.d3ddevice.Reset();
    sys->dec_device.d3d_dev.adapter.Reset();
    delete sys;
    return nullptr;
}

void D3D12_ReleaseDevice(d3d12_decoder_device_t *dev_sys)
{
    d3d12_decoder_device *sys = container_of(dev_sys, d3d12_decoder_device, dec_device);

    if ( sys->external.cleanupDeviceCb )
        sys->external.cleanupDeviceCb( sys->external.opaque );

    delete sys;
}


void D3D12_LogResources(const d3d12_device_t &d3d_dev)
{
#if !(defined(_GAMING_XBOX) || defined(_XBOX_ONE))
    Microsoft::WRL::ComPtr<ID3D12DebugDevice> debugDevice;
    if (SUCCEEDED(d3d_dev.d3ddevice.As(&debugDevice)))
    {
        debugDevice->ReportLiveDeviceObjects(D3D12_RLDO_IGNORE_INTERNAL | D3D12_RLDO_DETAIL);
    }
#endif
}


bool D3D12_DeviceSupportsFormat(d3d12_device_t *d3d_dev, DXGI_FORMAT format, UINT supportFlags)
{
    D3D12_FEATURE_DATA_FORMAT_SUPPORT support = { format, D3D12_FORMAT_SUPPORT1_NONE, D3D12_FORMAT_SUPPORT2_NONE };
    if FAILED( d3d_dev->d3ddevice->CheckFeatureSupport(D3D12_FEATURE_FORMAT_SUPPORT,
                                                 &support, sizeof(support)) )
        return false;
    return ( support.Support1 & supportFlags ) == supportFlags;
}

const d3d_format_t *FindD3D12Format(d3d12_device_t *d3d_dev,
                                    vlc_fourcc_t i_src_chroma,
                                    int rgb_yuv,
                                    uint8_t bits_per_channel,
                                    uint8_t widthDenominator,
                                    uint8_t heightDenominator,
                                    int cpu_gpu,
                                    UINT supportFlags)
{
    supportFlags |= D3D12_FORMAT_SUPPORT1_TEXTURE2D;
    for (const d3d_format_t *output_format = DxgiGetRenderFormatList();
         output_format->name != nullptr; ++output_format)
    {
        if (i_src_chroma && i_src_chroma != output_format->fourcc)
            continue;
        if (bits_per_channel && bits_per_channel > output_format->bitsPerChannel)
            continue;
        int cpu_gpu_fmt = is_d3d12_opaque(output_format->fourcc) ? DXGI_CHROMA_GPU : DXGI_CHROMA_CPU;
        if ((cpu_gpu & cpu_gpu_fmt)==0)
            continue;
        int format = vlc_fourcc_IsYUV(output_format->fourcc) ? DXGI_YUV_FORMAT : DXGI_RGB_FORMAT;
        if ((rgb_yuv & format)==0)
            continue;
        if (widthDenominator && widthDenominator < output_format->widthDenominator)
            continue;
        if (heightDenominator && heightDenominator < output_format->heightDenominator)
            continue;

        DXGI_FORMAT textureFormat;
        if (output_format->formatTexture == DXGI_FORMAT_UNKNOWN)
            textureFormat = output_format->resourceFormat[0];
        else
            textureFormat = output_format->formatTexture;

        if( D3D12_DeviceSupportsFormat( d3d_dev, textureFormat, supportFlags ) )
            return output_format;
    }
    return nullptr;
}

#ifndef NDEBUG
void (D3D12_ObjectSetName)(ID3D12Object *o, const wchar_t *name, ...)
{
    wchar_t tmp[256];
    va_list ap;
    va_start(ap, name);
    vswprintf_s(tmp, ARRAY_SIZE(tmp), name, ap);
    va_end(ap);
    o->SetName(tmp);
}
#endif


HRESULT D3D12_CommandListCreateGraphic(vlc_object_t *o, d3d12_device_t *d3d_dev, d3d12_commands_t *cmd)
{
    HRESULT hr;
    hr = d3d_dev->d3ddevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT,
                                            IID_GRAPHICS_PPV_ARGS(&cmd->m_commandAllocator));
    if (FAILED(hr))
    {
        msg_Err(o, "failed to create quad command allocator. (hr=0x%lX)", hr);
        goto error;
    }
    D3D12_ObjectSetName(cmd->m_commandAllocator, L"quad Command Allocator");

    hr = d3d_dev->d3ddevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT,
                                        cmd->m_commandAllocator, nullptr/*pipelines[i].m_pipelineState*/,
                                        IID_GRAPHICS_PPV_ARGS(&cmd->m_commandList));
    if (FAILED(hr))
    {
        msg_Err(o, "failed to create command list. (hr=0x%lX)", hr);
        goto error;
    }
    D3D12_ObjectSetName(cmd->m_commandList, L"quad Command List");

    // a command list is recording commands by default
    hr = cmd->m_commandList->Close();
    if (unlikely(FAILED(hr)))
    {
        msg_Err(o, "failed to close command list. (hr=0x%lX)", hr);
        goto error;
    }
    return hr;
error:
    return hr;
}


const struct vlc_video_context_operations d3d12_vctx_ops = {
    nullptr,
};

vlc_video_context *D3D12CreateVideoContext(vlc_decoder_device *dec_dev, DXGI_FORMAT vctx_fmt)
{
    vlc_video_context *vctx = vlc_video_context_Create( dec_dev, VLC_VIDEO_CONTEXT_D3D12VA,
                                          sizeof(d3d12_video_context_t), &d3d12_vctx_ops );
    if (unlikely(vctx == nullptr))
        return nullptr;

    d3d12_video_context_t *priv = GetD3D12ContextPrivate(vctx);
    priv->format = vctx_fmt;
    return vctx;
}

void AcquireD3D12PictureSys(picture_sys_d3d12_t *p_sys)
{
    for (int i=0; i<DXGI_MAX_SHADER_VIEW; i++) {
        if (p_sys->resource[i])
            p_sys->resource[i]->AddRef();
    }
}

void ReleaseD3D12PictureSys(picture_sys_d3d12_t *p_sys)
{
    for (int i=0; i<DXGI_MAX_SHADER_VIEW; i++) {
        if (p_sys->resource[i])
            p_sys->resource[i]->Release();
    }
}

picture_sys_d3d12_t *ActiveD3D12PictureSys(picture_t *pic)
{
    assert(pic->context != nullptr);
    assert(pic->p_sys == nullptr);
    struct d3d12_pic_context *pic_ctx = D3D12_PICCONTEXT_FROM_PICCTX(pic->context);
    return &pic_ctx->picsys;
}

void d3d12_pic_context_destroy(picture_context_t *ctx)
{
    struct d3d12_pic_context *pic_ctx = D3D12_PICCONTEXT_FROM_PICCTX(ctx);
    ReleaseD3D12PictureSys(&pic_ctx->picsys);
    free(pic_ctx);
}
