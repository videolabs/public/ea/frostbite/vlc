/*****************************************************************************
 * d3d12_device.c : D3D12 decoder device
 *****************************************************************************
 * Copyright © 2021 VLC authors, VideoLAN and VideoLabs
 *
 * Authors: Steve Lhomme <robux4@videolabs.io>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_codec.h>
#include <vlc_plugin.h>

#include "d3d12_fmt.h"

EXTERN_SYMBOL int D3D12OpenDecoderDevice(vlc_decoder_device *, vout_window_t *);

#ifndef FB_VLC_MODULES_AGGREGATE
vlc_module_begin()
    set_description(N_("Direct3D12 decoder device"))
    set_category( CAT_VIDEO )
    set_callback_dec_device( D3D12OpenDecoderDevice, 30 )
    add_shortcut ("d3d12")
vlc_module_end()
#endif // !FB_VLC_MODULES_AGGREGATE

static void D3D12CloseDecoderDevice(vlc_decoder_device *device)
{
    d3d12_decoder_device_t *dec_device = static_cast<d3d12_decoder_device_t *>(device->opaque);
    D3D12_ReleaseDevice(dec_device);
}

static const struct vlc_decoder_device_operations d3d12_dev_ops = {
    D3D12CloseDecoderDevice,
};

int D3D12OpenDecoderDevice(vlc_decoder_device *device, vout_window_t *wnd)
{
    VLC_UNUSED(wnd);

    d3d12_decoder_device_t *dec_device = D3D12_CreateDevice(&device->obj);
    if (dec_device == nullptr)
        return VLC_EGENERIC;

    device->ops = &d3d12_dev_ops;
    device->opaque = dec_device;
    device->type = VLC_DECODER_DEVICE_D3D12;
    device->sys = nullptr;

    return VLC_SUCCESS;
}
