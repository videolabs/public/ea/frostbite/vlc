#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc/libvlc.h>

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdalign.h>

#ifdef VLC_FORCED_ALLOCATOR_ENABLED
#error Invalid build configuration - expect infinite recursive calls induced stack overflows
#endif

static void* std_malloc(void *opaque, size_t size)
{
    (void)opaque;
    return malloc(size);
}

static void* std_calloc(void *opaque, size_t num, size_t size)
{
    (void)opaque;
    return calloc(num, size);
}

static void* std_realloc(void *opaque, void *ptr, size_t size)
{
    (void)opaque;
    return realloc(ptr, size);
}

static void std_free(void *opaque, void *ptr)
{
    (void)opaque;
    free(ptr);
}

static void* std_aligned_alloc(void *opaque, size_t align, size_t size)
{
    (void)opaque;
#if defined(HAVE_ALIGNED_ALLOC)
    return aligned_alloc(align, size);
#elif defined(HAVE_MEMALIGN)
    return memalign(align, size);
#elif defined (_WIN32) && defined(__MINGW32__)
    return __mingw_aligned_malloc(size, align);
#elif defined (_WIN32) && defined(_MSC_VER)
    return _aligned_malloc(size, align);
#else
    /* align must be valid/supported */
    assert(align <= alignof (max_align_t));
    return malloc(((void) align, size));
#endif
}

static void* std_aligned_realloc(void *opaque, void* ptr, size_t size, size_t align)
{
	(void)opaque;
#if defined(_WIN32) && defined(__MINGW32__)
    return __mingw_aligned_realloc(ptr, size, align);
#elif defined (_WIN32) && defined(_MSC_VER)
    return _aligned_realloc(ptr, size, align);
#else
    free(ptr);
    return NULL;
#endif
}

static void std_aligned_free(void *opaque, void *ptr)
{
    (void)opaque;
#if defined(HAVE_ALIGNED_ALLOC)
    free(ptr);
#elif defined(HAVE_MEMALIGN)
    free(ptr);
#elif defined (_WIN32) && defined(__MINGW32__)
    __mingw_aligned_free(ptr);
#elif defined (_WIN32) && defined(_MSC_VER)
    _aligned_free(ptr);
#else
    free(ptr);
#endif
}


static char* std_strdup(void* opaque, const char* str)
{
    (void)opaque;
    return strdup(str);
}

const struct libvlc_allocator default_allocator = 
{
    NULL,
    std_malloc,
    std_calloc,
    std_realloc,
    std_free,

    std_aligned_alloc,
    std_aligned_realloc,
    std_aligned_free,

    std_strdup,
};
