#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc/libvlc.h>

extern const struct libvlc_allocator default_allocator;

static const struct libvlc_allocator *global_allocators = &default_allocator;

void libvlc_set_allocator(struct libvlc_allocator *allocator)
{
    if (allocator != NULL)
        global_allocators = allocator;
    else
        global_allocators = &default_allocator;
}

const struct libvlc_allocator* libvlc_get_allocator(void)
{
    return global_allocators;
}

void* vlc_mem_malloc(size_t size)
{
    return global_allocators->p_malloc(global_allocators->opaque, size);
}

void* vlc_mem_calloc(size_t num, size_t size)
{
    return global_allocators->p_calloc(global_allocators->opaque, num, size);
}

void* vlc_mem_realloc(void *ptr, size_t size)
{
    return global_allocators->p_realloc(global_allocators->opaque, ptr, size);
}

void* vlc_mem_aligned_alloc(size_t align, size_t size)
{
    return global_allocators->p_aligned_alloc(global_allocators->opaque, align, size);
}

void* vlc_mem_aligned_realloc(void* ptr, size_t size, size_t align)
{
    return global_allocators->p_aligned_realloc(global_allocators->opaque, ptr, size, align);
}

void vlc_mem_aligned_free(void *ptr)
{
    global_allocators->p_aligned_free(global_allocators->opaque, ptr);
}

void vlc_mem_free(void *ptr)
{
    global_allocators->p_free(global_allocators->opaque, ptr);
}

char* vlc_mem_strdup(const char *str)
{
    return global_allocators->p_strdup(global_allocators->opaque, str);
}
