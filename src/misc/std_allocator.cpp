#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#ifdef VLC_FORCED_ALLOCATOR_ENABLED
#error Invalid build configuration - expect infinite recursive calls induced stack overflows
#endif

#include <vlc_common.h>
#include <vlc_mem.h>

#include <new>

void* operator new(size_t size)
{
    return vlc_mem_malloc(size);
}

void* operator new[](size_t size)
{
    return vlc_mem_malloc(size);
}

void* operator new(size_t size, std::nothrow_t&) noexcept
{
    return vlc_mem_malloc(size);
}

void* operator new[](size_t size, std::nothrow_t&) noexcept
{
    return vlc_mem_malloc(size);
}

void operator delete(void* ptr) noexcept
{
    vlc_mem_free(ptr);
}

void operator delete[](void* ptr) noexcept
{
    vlc_mem_free(ptr);
}

#if !defined(_MSC_VER)
void *operator new(size_t sz, const libvlc_allocator *)
{
    return vlc_mem_malloc(sz);
}

void operator delete(void *ptr, const libvlc_allocator *)
{
    // Do nothing like the standard library
    // This is only call with exceptions during new
}
#endif // !_MSC_VER
