/*****************************************************************************
 * thread.c : pthread back-end for LibVLC
 *****************************************************************************
 * Copyright (C) 1999-2021 VLC authors and VideoLAN
 *
 * Authors: Jean-Marc Dressler <polux@via.ecp.fr>
 *          Samuel Hocevar <sam@zoy.org>
 *          Gildas Bazin <gbazin@netcourrier.com>
 *          Clément Sténac
 *          Rémi Denis-Courmont
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc/libvlc.h>

#include "libvlc.h"
#include <stdarg.h>
#include <stdatomic.h>
#include <stdnoreturn.h>
#include <errno.h>
#include <time.h>
#include <assert.h>
#include <unistd.h>

static unsigned vlc_clock_prec;

static void vlc_clock_setup_once (void)
{
    struct timespec res;
    if (unlikely(clock_getres(CLOCK_MONOTONIC, &res) != 0 || res.tv_sec != 0))
        abort ();
    vlc_clock_prec = (res.tv_nsec + 500) / 1000;
}

/**
 * Print a backtrace to the standard error for debugging purpose.
 */
void vlc_trace (const char *fn, const char *file, unsigned line)
{
     fprintf (stderr, "at %s:%u in %s\n", file, line, fn);
     fflush (stderr); /* needed before switch to low-level I/O */
#ifdef HAVE_BACKTRACE
     void *stack[20];
     int len = backtrace (stack, ARRAY_SIZE (stack) );
     backtrace_symbols_fd (stack, len, 2);
#endif
     fsync (2);
}

#ifndef NDEBUG
/**
 * Reports a fatal error from the threading layer, for debugging purposes.
 */
static void
vlc_thread_fatal (const char *action, int error,
                  const char *function, const char *file, unsigned line)
{
    int canc = vlc_savecancel ();
    fprintf (stderr, "LibVLC fatal error %s (%d) in thread %lu ",
             action, error, vlc_thread_id ());
    vlc_trace (function, file, line);
    perror ("Thread error");
    fflush (stderr);

    vlc_restorecancel (canc);
    abort ();
}

# define VLC_THREAD_ASSERT( action ) \
    if (unlikely(val)) \
        vlc_thread_fatal (action, val, __func__, __FILE__, __LINE__)
#else
# define VLC_THREAD_ASSERT( action ) ((void)val)
#endif

int vlc_threadvar_create (vlc_threadvar_t *key, void (*destr) (void *))
{
    return scePthreadKeyCreate (key, destr);
}

void vlc_threadvar_delete (vlc_threadvar_t *p_tls)
{
    scePthreadKeyDelete (*p_tls);
}

int vlc_threadvar_set (vlc_threadvar_t key, void *value)
{
    return scePthreadSetspecific (key, value);
}

void *vlc_threadvar_get (vlc_threadvar_t key)
{
    return scePthreadGetspecific (key);
}

void vlc_threads_setup (libvlc_int_t *p_libvlc)
{
    (void) p_libvlc;
}

static thread_hook_start_t g_threadHookStart = NULL;
static thread_hook_stop_t g_threadHookStop = NULL;

void libvlc_set_thread_hook(thread_hook_start_t startHook, thread_hook_start_t stopHook)
{
    g_threadHookStart = startHook;
    g_threadHookStop = stopHook;
}

static void* thread_entry_point(void* data)
{
    vlc_thread_t* th = (vlc_thread_t*)data;

    if (g_threadHookStart)
        g_threadHookStart(th->name);

    void* ret = th->entry_point(th->thread_data);

    if (g_threadHookStop)
        g_threadHookStop();

    return ret;
}

static int vlc_clone_attr (vlc_thread_t *th, ScePthreadAttr *attr,
                           void *(*entry) (void *), void *data, int priority, const char *name)
{
    int ret;

    th->entry_point = entry;
    th->thread_data = data;
    th->name = name;

    ret = scePthreadCreate(&th->handle, attr, thread_entry_point, th, name);
    scePthreadAttrDestroy (attr);
    (void) priority;
    return ret;
}

int vlc_clone (vlc_thread_t *th, void *(*entry) (void *), void *data,
               int priority, const char *name)
{
    ScePthreadAttr attr;

    scePthreadAttrInit (&attr);
    return vlc_clone_attr (th, &attr, entry, data, priority, name);
}

void vlc_join(vlc_thread_t th, void **result)
{
    int val = scePthreadJoin(th.handle, result);
    VLC_THREAD_ASSERT ("joining thread");
}

VLC_WEAK unsigned long vlc_thread_id(void)
{
    return (unsigned long)scePthreadGetthreadid();
}

int vlc_set_priority (vlc_thread_t th, int priority)
{
    (void) th; (void) priority;
    return VLC_SUCCESS;
}

void vlc_cancel(vlc_thread_t th)
{
    scePthreadCancel(th.handle);
}

int vlc_savecancel (void)
{
    int state;
    int val = scePthreadSetcancelstate(PTHREAD_CANCEL_DISABLE, &state);

    VLC_THREAD_ASSERT ("saving cancellation");
    return state;
}

void vlc_restorecancel (int state)
{
#ifndef NDEBUG
    int oldstate, val;

    val = scePthreadSetcancelstate (state, &oldstate);
    /* This should fail if an invalid value for given for state */
    VLC_THREAD_ASSERT ("restoring cancellation");

    if (unlikely(oldstate != SCE_PTHREAD_CANCEL_DISABLE))
         vlc_thread_fatal ("restoring cancellation while not disabled", EINVAL,
                           __func__, __FILE__, __LINE__);
#else
    scePthreadSetcancelstate (state, NULL);
#endif
}

void vlc_testcancel (void)
{
    scePthreadTestcancel ();
}

vlc_tick_t vlc_tick_now (void)
{
    struct timespec ts;

    if (unlikely(clock_gettime(CLOCK_MONOTONIC, &ts) != 0))
        abort ();

    return vlc_tick_from_timespec( &ts );
}

#undef vlc_tick_wait
void vlc_tick_wait (vlc_tick_t deadline)
{
    static ScePthreadOnce  vlc_clock_once = SCE_PTHREAD_ONCE_INIT;

    /* If the deadline is already elapsed, or within the clock precision,
     * do not even bother the system timer. */
    scePthreadOnce(&vlc_clock_once, vlc_clock_setup_once);
    deadline -= vlc_clock_prec;

    struct timespec ts = timespec_from_vlc_tick (deadline);

    while (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL) == EINTR);
}

#undef vlc_tick_sleep
void vlc_tick_sleep (vlc_tick_t delay)
{
    struct timespec ts = timespec_from_vlc_tick (delay);

    while (clock_nanosleep(CLOCK_MONOTONIC, 0, &ts, &ts) == EINTR);
}

unsigned vlc_GetCPUCount(void)
{
    return 4;
}
