/*****************************************************************************
 * rand.c : PS5 non-predictible random bytes generator
 *****************************************************************************
 * Copyright © 2021 VideoLAN and VLC authors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdlib.h>

#include <vlc_common.h>
#include <vlc_threads.h>

void vlc_rand_bytes (void *buf, size_t len)
{
    static vlc_mutex_t vlc_rand_lock = VLC_STATIC_MUTEX;
    static bool init = false;
    static int seed = 0;
    
    vlc_mutex_lock(&vlc_rand_lock);
    if (!init)
    {
        seed = time(0);
        init = true;
    }
    
    char* cbuf = buf;
    while (len)
    {
        int rnd = rand_r(&seed);
        size_t copy;
        if (len > sizeof(int))
            copy = sizeof(int);
        else
            copy = len;
        memcpy(cbuf, &rnd, copy);
        len -= copy;
        cbuf += copy;
    }
}
