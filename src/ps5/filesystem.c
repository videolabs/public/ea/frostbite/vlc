/*****************************************************************************
 * filesystem.c: PS5 file system helpers
 *****************************************************************************
 * Copyright (C) 2005-2021 VLC authors and VideoLAN
 * Copyright © 2005-2008 Rémi Denis-Courmont
 *
 * Authors: Rémi Denis-Courmont
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>

#include <stdio.h>
#include <limits.h> /* NAME_MAX */
#include <errno.h>

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#include <vlc_common.h>
#include <vlc_fs.h>

#if !defined(HAVE_ACCEPT4)
static inline void vlc_cloexec(int fd)
{
    fcntl(fd, F_SETFD, FD_CLOEXEC | fcntl(fd, F_GETFD));
}
#endif
#if !defined(MSG_NOSIGNAL) && defined(SO_NOSIGPIPE)
# define MSG_NOSIGNAL 0
#endif

int vlc_open (const char *filename, int flags, ...)
{
    unsigned int mode = 0;
    va_list ap;

    va_start (ap, flags);
    if (flags & O_CREAT)
        mode = va_arg (ap, unsigned int);
    va_end (ap);

    return open(filename, flags | O_CLOEXEC, mode);
}

int vlc_openat (int dir, const char *filename, int flags, ...)
{
    unsigned int mode = 0;
    va_list ap;

    va_start (ap, flags);
    if (flags & O_CREAT)
        mode = va_arg (ap, unsigned int);
    va_end (ap);

    return openat(dir, filename, flags | O_CLOEXEC, mode);
}

#ifdef HAVE_MKOSTEMP
int vlc_mkstemp (char *template)
{
    return mkostemp(template, O_CLOEXEC);
}
#endif

VLC_WEAK int vlc_memfd(void)
{
    char bufpath[] = "/tmp/"PACKAGE_NAME"XXXXXX";
    int fd;

    fd = vlc_mkstemp (bufpath);
    if (fd != -1)
        unlink (bufpath);
    return fd;
}

int vlc_close (int fd)
{
    int ret;
#ifdef POSIX_CLOSE_RESTART
    ret = posix_close(fd, 0);
#else
    ret = close(fd);
    /* POSIX.2008 (and earlier) does not specify if the file descriptor is
     * closed on failure. Assume it is as on Linux and most other common OSes.
     * Also emulate the correct error code as per newer POSIX versions. */
    if (unlikely(ret != 0) && unlikely(errno == EINTR))
        errno = EINPROGRESS;
#endif
    assert(ret == 0 || errno != EBADF); /* something is corrupt? */
    return ret;
}

int vlc_stat (const char *filename, struct stat *buf)
{
    return stat (filename, buf);
}

int vlc_lstat (const char *filename, struct stat *buf)
{
    return lstat (filename, buf);
}

int vlc_unlink (const char *filename)
{
    return unlink (filename);
}

int vlc_rename (const char *oldpath, const char *newpath)
{
    return rename (oldpath, newpath);
}

char *vlc_getcwd (void)
{
    long path_max = pathconf (".", _PC_PATH_MAX);
    size_t size = (path_max == -1 || path_max > 4096) ? 4096 : path_max;

    for (;; size *= 2)
    {
        char *buf = malloc (size);
        if (unlikely(buf == NULL))
            break;

        if (getcwd (buf, size) != NULL)
            return buf;
        free (buf);

        if (errno != ERANGE)
            break;
    }
    return NULL;
}

int vlc_dup (int oldfd)
{
    return fcntl (oldfd, F_DUPFD_CLOEXEC, 0);
}

int vlc_dup2(int oldfd, int newfd)
{
#ifdef HAVE_DUP3
    return dup3(oldfd, newfd, O_CLOEXEC);
#else
    int ret = dup2(oldfd, newfd);
    if (ret >= 0)
        vlc_cloexec(newfd);
    return ret;
#endif
}

int vlc_pipe (int fds[2])
{
#ifdef HAVE_PIPE2
    return pipe2(fds, O_CLOEXEC);
#else
    int ret = pipe(fds);
    if (ret == 0)
    {
        vlc_cloexec(fds[0]);
        vlc_cloexec(fds[1]);
    }
    return ret;
#endif
}

ssize_t vlc_write(int fd, const void *buf, size_t len)
{
    struct iovec iov = { .iov_base = (void *)buf, .iov_len = len };

    return vlc_writev(fd, &iov, 1);
}
