#ifndef COMPAT_PSX_FIXUPS
# define COMPAT_PSX_FIXUPS 1

#ifndef __SCE__
# error "This header is for PS5 builds only"
#endif

#ifndef __cplusplus
typedef struct
{
    long long max_align1 __attribute__((__aligned__(__alignof__(long long))));
    long double max_align2 __attribute__((__aligned__(__alignof__(long double))));
} max_align_t;

#define static_assert _Static_assert

int clock_nanosleep(clockid_t clock_id, int flags,
        const struct timespec *rqtp, struct timespec *rmtp);

#endif

#endif
