#ifndef VLC_COMPAT_STDALIGN_H
# define VLC_COMPAT_STDALIGN_H 1

#ifndef __cplusplus
# define alignof _Alignof
#endif

#if __has_include_next(<stdalign.h>)
# include_next <stdalign.h>
#endif

#endif