#ifndef VLC_COMPAT_STDNORETURN_H
#define VLC_COMPAT_STDNORETURN_H 1

#define noreturn __attribute__((noreturn))

#endif