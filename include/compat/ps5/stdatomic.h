#ifndef COMPAT_STDATOMIC_H
# define COMPAT_STDATOMIC_H

#include <stdbool.h>
#include <stdint.h>

enum memory_order {
    memory_order_relaxed = __ATOMIC_RELAXED,
    memory_order_consume = __ATOMIC_CONSUME,
    memory_order_acquire = __ATOMIC_ACQUIRE,
    memory_order_release = __ATOMIC_RELEASE,
    memory_order_acq_rel = __ATOMIC_ACQ_REL,
    memory_order_seq_cst = __ATOMIC_SEQ_CST
};

typedef _Atomic(unsigned int) atomic_uint;
typedef _Atomic(int) atomic_int;
typedef _Atomic(bool) atomic_bool;
typedef _Atomic(uintmax_t) atomic_uintmax_t;
typedef _Atomic(uintptr_t) atomic_uintptr_t;
typedef _Atomic(unsigned char) atomic_uchar;

#define ATOMIC_VAR_INIT(value) (value)

#define atomic_init(obj, desired) __c11_atomic_init((obj), (desired))
#define atomic_load(obj) __c11_atomic_load((obj), __ATOMIC_SEQ_CST)
#define atomic_load_explicit(obj, order) __c11_atomic_load((obj), (order))
#define atomic_store(obj, desired) __c11_atomic_store((obj), (desired), __ATOMIC_SEQ_CST)
#define atomic_store_explicit(obj, desired, order) __c11_atomic_store((obj), (desired), (order))
#define atomic_fetch_add(obj, op) __c11_atomic_fetch_add((obj), (op), __ATOMIC_SEQ_CST)
#define atomic_fetch_add_explicit(obj, op, order) __c11_atomic_fetch_add((obj), (op), (order))
#define atomic_fetch_sub(obj, op)  __c11_atomic_fetch_sub((obj), (op), __ATOMIC_SEQ_CST)
#define atomic_fetch_sub_explicit(obj, op, order)  __c11_atomic_fetch_sub((obj), (op), (order))
#define atomic_exchange_explicit(obj, op, order) __c11_atomic_exchange((obj), (op), (order))
#define atomic_compare_exchange_weak_explicit(obj, exp, val, ord1, ord2) __c11_atomic_compare_exchange_weak((obj), (exp), (val), (ord1), (ord2))
#define atomic_compare_exchange_strong(obj, exp, val) __c11_atomic_compare_exchange_strong((obj), (exp), (val), __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#define atomic_compare_exchange_strong_explicit(obj, exp, val, ord1, ord2) __c11_atomic_compare_exchange_strong((obj), (exp), (val), (ord1), (ord2))
#define atomic_fetch_or_explicit(obj, val, order) __c11_atomic_fetch_or((obj), (val), (order))
#define atomic_exchange(obj, desired) __c11_atomic_exchange((obj), (desired), __ATOMIC_SEQ_CST)
#endif	