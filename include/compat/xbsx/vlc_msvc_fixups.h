#ifdef _MSC_VER /* help visual studio compile vlc headers */

#include <xkeycheck.h>

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif /* WIN32_LEAN_AND_MEAN */

#include <vlc_ms_fixups.h>

#if 0
#ifndef NOUSER
#define NOUSER /* avoid bogus pragma push/pop */
typedef struct tagMSG MSG, *LPMSG;
#endif /* NOUSER */
#endif

#ifndef WIN32
#define WIN32
#endif /* WIN32 */

#if _MSC_VER >= 1900 && !defined(__STDC_VERSION__)
#define __STDC_VERSION__ 199901L
#endif

# if !defined(snprintf)
#  define HAVE_SNPRINTF 1 /* bogus autoconf detection using a define */
#  define snprintf _snprintf
# endif
# if !defined(strdup)
#  define HAVE_STRDUP 1 /* bogus autoconf detection using a define */
# define strdup _strdup
#endif

#ifndef fdopen
# define fdopen _fdopen
#endif

//# define N_(x) x
//# define _(x) x

#if _MSC_VER < 1900
# ifndef __cplusplus
#  define inline __inline
# endif
#endif
#define __inline__ __inline
#ifndef __cplusplus
# define _Thread_local __declspec( thread )
#endif

//#if _MSC_VER < 1900 && defined(__cplusplus)
///* C++ 11 compatibility tricks */
//# define constexpr const
//# define alignof(x) __alignof(x)
//#endif

# define alloca _alloca

#define __func__ __FUNCDNAME__
# if !defined(vsnprintf) && !defined(__cplusplus) && _MSC_VER < 1900
#  define HAVE_VSNPRINTF 1 /* bogus autoconf detection using a define */
#  define vsnprintf _vsnprintf
# endif

#ifndef NOMINMAX
# define NOMINMAX
#endif

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif /* _CRT_SECURE_NO_WARNINGS */

#ifndef _CRT_NONSTDC_NO_WARNINGS
#define _CRT_NONSTDC_NO_WARNINGS
#endif /* _CRT_NONSTDC_NO_WARNINGS */

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif /* _USE_MATH_DEFINES */

#define PATH_MAX MAX_PATH

/* avoid compilation issues in ARM */
#ifndef _ARM_WINAPI_PARTITION_DESKTOP_SDK_AVAILABLE
#define _ARM_WINAPI_PARTITION_DESKTOP_SDK_AVAILABLE 1
#endif

#if defined(_DEBUG) && 0
#define _CRTDBG_MAP_ALLOC
#endif
#include <stdlib.h>
#if defined(_DEBUG) && 0
#include <crtdbg.h>
//#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
//#define new DEBUG_NEW
#endif

typedef unsigned int u_int;

#if (defined(_GAMING_XBOX_SCARLETT) || defined(_GAMING_XBOX_XBOXONE))

/* These functions are declared but not defined when building for Xbox1 Serie X */
#ifndef __cplusplus
#define read _read
#define write _write
#define close _close
#define dup _dup
#define dup2 _dup2
#define setmode _setmode
#define lseek _lseek
#endif

/* Some flags or defines are not available when building with WINAPI_FAMILY_GAMES, while the
* functions using those flags are avaialble.
* Work around this by redefining the flags we need
*/
#define IDN_ALLOW_UNASSIGNED        0x01  // Allow unassigned "query" behavior per RFC 3454

#endif

#pragma warning(disable:4201) /* nameless structs */
#pragma warning(disable:4242)
#pragma warning(disable:4244)
#pragma warning(disable : 4267)
#pragma warning(disable : 4152)
#pragma warning(disable: 4701) /* potentially uninitialized local variable used */
#pragma warning(disable: 4996) /* Deprecated functions */
#pragma warning(disable: 4232) /* Allow non static values with declspec(dllimport) as pointer to function */
#pragma warning(disable: 4646) /* noreturn function with return type != void */
#pragma warning(disable: 4114) /* same type qualifier used more than once */
#pragma warning(disable: 4189) /* Variable is initialized but not referenced */
#pragma warning(disable: 4458) /* Member variable shadowed */
#pragma warning(disable: 4200) /* 0-length array */
#pragma warning(disable: 4706) /* Assignment in conditional expression */
#pragma warning(disable: 4702) /* Unreachable code */
#pragma warning(disable: 4305) /* Truncation from double to float */
#pragma warning(disable: 4146) /* Unary minus applied to unsigned; result still unsigned */
#pragma warning(disable: 4115) /* Type declaration in parenthesis, which happens in windows SDK headers */
#endif
