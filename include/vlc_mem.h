/*****************************************************************************
 * vlc_mem.h: memory allocation to use in VLC and modules
 *
 * The functions names are vlc_mem_xxx but the header can be used by external
 * code as long as it's linked to the vlccore library (statically or dllimport)
 *****************************************************************************/

#ifndef VLC_MEM_H
# define VLC_MEM_H 1

#include <stddef.h>

// VLC_C_FUNC == VLC_EXTERN
#ifdef __cplusplus
# define VLC_C_FUNC extern "C"
#else // !__cplusplus
# define VLC_C_FUNC
#endif // !__cplusplus

// VLC_EXPORTING == VLC_EXPORT
#if (defined (_WIN32) || defined(__SCE__)) && defined (DLL_EXPORT)
# define VLC_EXPORTING __declspec(dllexport)
#elif defined (__GNUC__)
# define VLC_EXPORTING __attribute__((visibility("default")))
#else
# define VLC_EXPORTING
#endif

// VLC_EXPORTED == VLC_API
#define VLC_EXPORTED  VLC_C_FUNC VLC_EXPORTING

// VLC_MEMORY_USED == VLC_USED
# if defined(_MSC_VER)
#  include <specstrings.h>
#  define VLC_MEMORY_USED      _Must_inspect_result_
# elif defined(__GNUC__)
#  define VLC_MEMORY_USED      __attribute__ ((warn_unused_result))
# else // !_MSC_VER && !__GNUC__
#  define VLC_MEMORY_USED
# endif // !_MSC_VER && !__GNUC__


/*
 * Internal VLC allocators
 */
VLC_EXPORTED VLC_MEMORY_USED void* vlc_mem_malloc(size_t size);
VLC_EXPORTED VLC_MEMORY_USED void* vlc_mem_calloc(size_t num, size_t size);
VLC_EXPORTED VLC_MEMORY_USED void* vlc_mem_realloc(void *ptr, size_t size);
VLC_EXPORTED void vlc_mem_free(void* ptr);

VLC_EXPORTED VLC_MEMORY_USED void* vlc_mem_aligned_alloc(size_t align, size_t size);
VLC_EXPORTED VLC_MEMORY_USED void* vlc_mem_aligned_realloc(void *ptr, size_t size, size_t align);
VLC_EXPORTED void vlc_mem_aligned_free(void* ptr);

VLC_EXPORTED VLC_MEMORY_USED char* vlc_mem_strdup(const char *str);

#if defined(__cplusplus) && !defined(VLC_FORCED_ALLOCATOR_ENABLED)
// only used for code using libvlc
#include <new>
#include <vlc/libvlc.h>

#if defined(_MSC_VER)
inline VLC_MEMORY_USED void *operator new(size_t sz, const libvlc_allocator *)
{
    return vlc_mem_malloc(sz);
}

inline VLC_MEMORY_USED void *operator new(size_t sz, std::align_val_t alignment, const libvlc_allocator *)
{
    return vlc_mem_aligned_alloc((size_t)alignment, sz);
}

inline VLC_MEMORY_USED void *operator new[](size_t sz, const libvlc_allocator *)
{
    return vlc_mem_malloc(sz);
}

inline VLC_MEMORY_USED void *operator new[](size_t sz, std::align_val_t alignment, const libvlc_allocator *)
{
    return vlc_mem_aligned_alloc((size_t)alignment, sz);
}

inline void operator delete(void *, const libvlc_allocator *)
{
    // Do nothing like the standard library
    // This is only call with exceptions during new
}

inline void operator delete(void*, std::align_val_t, const libvlc_allocator*)
{
    // Do nothing like the standard library
    // This is only call with exceptions during new
}
#else // !_MSC_VER
VLC_EXPORTED VLC_MEMORY_USED void *operator new(size_t sz, const libvlc_allocator *);
VLC_EXPORTED void operator delete(void *ptr, const libvlc_allocator *);
#endif // !_MSC_VER

template<typename T>
void vlc_mem_delete(T *obj)
{
    if (obj)
    {
        obj->~T();
#if defined(_MSC_VER)
        vlc_mem_aligned_free(obj);
#else
        // needs to match the allocator used in std_allocator.cpp
        vlc_mem_free(obj);
#endif
    }
}

#endif


#endif /* !VLC_MEM_H */
