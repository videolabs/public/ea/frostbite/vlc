/*
 * Replace standard allocator/string calls with vlc_mem_xxx calls which can
 * redirect to custom allocators.
 */

#ifndef VLC_FORCED_ALLOCATOR_H
#define VLC_FORCED_ALLOCATOR_H

#define VLC_FORCED_ALLOCATOR_ENABLED 1

#include "vlc_mem.h"

#ifdef __cplusplus
#include <new>

// override C++ basic allocation in libvlc
void* operator new(size_t size);
void* operator new[](size_t size);
void* operator new(size_t size, std::nothrow_t& ) noexcept;
void* operator new[](size_t size, std::nothrow_t& ) noexcept;
void operator delete(void* p) noexcept;
void operator delete[](void* p) noexcept;

#include <cstdlib>
#include <cstring>

#else  // !__cplusplus

#include <stdlib.h>
#include <string.h>

#endif // !__cplusplus


// define the name rather than the call because some callbacks in VLC and
// contribs are called free/malloc/realloc/calloc and the calls end up using
// the vlc_mem_xxx callback which is not defined as such

#undef malloc
#define malloc                     vlc_mem_malloc
#undef calloc
#define calloc                     vlc_mem_calloc
#undef realloc
#define realloc                    vlc_mem_realloc
#undef free
#define free                       vlc_mem_free


// aligned allocations don't have these names as callbacks so can be used
// calls.

#undef aligned_alloc
#define aligned_alloc(a, s)        vlc_mem_aligned_alloc( (a), (s) )
#undef aligned_free
#define aligned_free(p)            vlc_mem_aligned_free( (void*) (p) )
// MSVC style aligned allocations
#undef _aligned_malloc
#define _aligned_malloc(a, s)      vlc_mem_aligned_alloc( (s), (a) )
#undef _aligned_free
#define _aligned_free(p)           vlc_mem_aligned_free( (void*) (p) )
#undef _aligned_realloc
#define _aligned_realloc(p, s, a)  vlc_mem_aligned_realloc( (p), (s), (a) )


// string functions expected to call free() with the returned value

#undef strdup
#define strdup(s)                  vlc_mem_strdup( (s) )


#endif  // VLC_FORCED_ALLOCATOR_H
