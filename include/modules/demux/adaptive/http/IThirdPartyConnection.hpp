#ifndef ITHIRDPARTYCONNECTION_HPP
#define ITHIRDPARTYCONNECTION_HPP

#include <string>

namespace adaptive
{
namespace http
{

class IThirdPartyConnectionCb
{
public:
    virtual ~IThirdPartyConnectionCb() = default;
    virtual void onCookieReceived( const std::string& cookie,
                                   const std::string& url ) = 0;
};

class IThirdPartyConnection
{
public:
    virtual ~IThirdPartyConnection() = default;
    virtual void disconnect() = 0;
    virtual void setRange( int64_t begin, int64_t end ) = 0;
    virtual int request( const char *url, IThirdPartyConnectionCb* cb ) = 0;
    virtual ssize_t read( void* buffer, size_t length ) = 0;
    virtual const char* contentType() const = 0;
    virtual size_t contentLength() const = 0;
};

}
}

#endif // ITHIRDPARTYCONNECTION_HPP
